if (!place_meeting(x + argument0, y, oWall)) {
	x += argument0
} else {
	while(!place_meeting(x + sign(argument0), y, oWall)) {
		x += sign(argument0)
	}
}
if (!place_meeting(x, y + argument1, oWall)) {
	y += argument1
} else {
	while(!place_meeting(x, y + sign(argument1), oWall)) {
		y += sign(argument1)
	}
}

move_snap(1, 1)