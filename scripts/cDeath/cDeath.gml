oCrab.dead = true

oControl.pause = true
oControl.inDialog = true
oControl.dialogIndex = 0
oControl.canTalk = false


dialogList = ds_list_create()

dialog = instance_create_depth(x, y, depth, oDialogBox)
dialog.line1 = "Your shell is broken, and you're temporarily homeless."
ds_list_add(dialogList, dialog)

dialog = instance_create_depth(x, y, depth, oDialogBox)
dialog.line1 = "Fortunately, there's an old boot over there which you could"
dialog.line2 = "use as a new home."
ds_list_add(dialogList, dialog)

oControl.dialogList = dialogList