event_inherited()

if (oControl.shell3.traded) {
	shell = oControl.shell3
} else {
	shell = oControl.shell4	
}

shell.x = x
shell.y = y + 18
shell.depth = depth + 1
