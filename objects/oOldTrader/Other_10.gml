event_inherited()

dialogList = ds_list_create()

natureCount = 0
for (i = 0; i < ds_list_size(oCrab.shell.shinies); i++){
	if (ds_list_find_value(oCrab.shell.shinies,i).isNatural){
		natureCount += 1
	}
}

if (oControl.shell3.traded) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "My new shell takes me back to the happy days of my youth."
	ds_list_add(dialogList, dialog)
} else if (oCrab.shell != oControl.shell3) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I've been told I have the most beautiful shell on the beach."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "But sometimes I long for a simpler life."
	ds_list_add(dialogList, dialog)
} else if (natureCount < 3) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I've been told I have the most beautiful shell on the beach."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "But sometimes I long for a simpler life."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Your shell has a rustic charm. It reminds me of my"
	dialog.line2 = "childhood in the coral reef."
	ds_list_add(dialogList, dialog)
} else {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I've been told I have the most beautiful shell on the beach."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "But sometimes I long for a simpler life."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Your shell looks just like the shell I had when I was"
	dialog.line2 = "younger. Please, would you trade your shell for mine?"
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Thank you. I'm so grateful!"
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "If you showed that shell to anyone who teased you before,"
	dialog.line2 = "you'd really put them in their place."
	ds_list_add(dialogList, dialog)
	
	oControl.shell3.traded = true
	oCrab.shell = oControl.shell4
	shell = oControl.shell3
	shell.x = x
	shell.y = y + 18
	shell.depth = depth + 1
}

oControl.dialogList = dialogList
