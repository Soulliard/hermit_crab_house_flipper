{
    "id": "ac3d8233-b131-4e3b-b93c-6de9ad89674d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOldTrader",
    "eventList": [
        {
            "id": "f15b01a0-2028-4d80-8b9e-54084f43855a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ac3d8233-b131-4e3b-b93c-6de9ad89674d"
        },
        {
            "id": "20d12163-3f2e-4185-b30e-07d22ac09b7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac3d8233-b131-4e3b-b93c-6de9ad89674d"
        }
    ],
    "maskSpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "overriddenProperties": null,
    "parentObjectId": "2432e09a-5410-407e-8db6-2f2ad0984227",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27f74558-22a4-43ef-b09f-2f3e105cde3e",
    "visible": true
}