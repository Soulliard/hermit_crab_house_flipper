{
    "id": "77064a8a-95aa-4397-8cfc-2c49a1391513",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShell1",
    "eventList": [
        {
            "id": "ea838b4a-12f9-4c5c-b41d-77c697b58cf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77064a8a-95aa-4397-8cfc-2c49a1391513"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ef6a6a6e-941e-43db-878c-4b51dffda250",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dda1ff83-0182-4df5-bf6f-6e9725f7c577",
    "visible": true
}