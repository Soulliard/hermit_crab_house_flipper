{
    "id": "dabb6d75-8b21-4a32-9c6a-e0150db8db7e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCoral2",
    "eventList": [
        {
            "id": "8956b6b4-1a50-4e89-b5a9-69b65f00deaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dabb6d75-8b21-4a32-9c6a-e0150db8db7e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "114260c3-2596-412a-956a-89afdca54372",
    "visible": true
}