{
    "id": "e74e9057-71e4-4785-bdc5-9b878b17e831",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCanTab",
    "eventList": [
        {
            "id": "0d05a633-738b-4cf3-8e0c-d3c051be1a50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e74e9057-71e4-4785-bdc5-9b878b17e831"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "95420960-a33c-4397-b5f8-613e1a967256",
    "visible": true
}