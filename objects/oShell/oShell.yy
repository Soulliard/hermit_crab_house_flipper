{
    "id": "ef6a6a6e-941e-43db-878c-4b51dffda250",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShell",
    "eventList": [
        {
            "id": "76563d5b-de65-42ee-ac0c-bb5216c5cbd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef6a6a6e-941e-43db-878c-4b51dffda250"
        },
        {
            "id": "3fa4707a-ad31-4033-8af1-f575eafa0826",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ef6a6a6e-941e-43db-878c-4b51dffda250"
        },
        {
            "id": "af46617c-b5e7-4d19-b227-2e56b8f5bae7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef6a6a6e-941e-43db-878c-4b51dffda250"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}