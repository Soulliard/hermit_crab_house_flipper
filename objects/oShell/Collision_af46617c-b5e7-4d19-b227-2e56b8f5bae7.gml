/// @description Insert description here
// You can write your code in this editor

var i;
var hasType = false;
for (i = 0; i < ds_list_size(shinies); i++){
	if (ds_list_find_value(shinies,i).shinyType == other.shinyType){
		hasType = true;
	}
}

if (!hasType) {
	shellShiny = instance_create_depth(x, y, depth, oShellShiny)
	shellShiny.shinyType = other.shinyType
	shellShiny.isSpooky = other.isSpooky
	shellShiny.isTrash = other.isTrash
	shellShiny.isNatural = other.isNatural
	shellShiny.isBling = other.isBling
	shellShiny.sprite_index = other.shellSprite
	shellShiny.depthOffset = other.depthOffset

	ds_list_add(shinies, shellShiny)
	audio_play_sound(itemsound,3,false)
	
	with (other) {
		instance_destroy()
	}
}

