{
    "id": "679b11ca-a5f3-48c7-b370-182cd401a2c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShellUrchin",
    "eventList": [
        {
            "id": "11389043-94ab-4077-bc82-c947dca49d4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "679b11ca-a5f3-48c7-b370-182cd401a2c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b6cd97c2-3200-47c2-9ec8-7a9190fc0e20",
    "visible": true
}