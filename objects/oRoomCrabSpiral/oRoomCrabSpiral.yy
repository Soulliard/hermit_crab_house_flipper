{
    "id": "64bfbd6d-d918-436e-9380-3e149778a1ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRoomCrabSpiral",
    "eventList": [
        {
            "id": "515a7f04-54d9-462c-8ebf-68db1f26b9f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64bfbd6d-d918-436e-9380-3e149778a1ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8a118abb-8326-468f-b0c1-86bb46c0f6be",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}