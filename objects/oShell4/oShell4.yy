{
    "id": "2540e7a5-3cbb-48b5-b6ad-8177e12b4700",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShell4",
    "eventList": [
        {
            "id": "84e12352-b095-426f-9019-f92df5f442f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2540e7a5-3cbb-48b5-b6ad-8177e12b4700"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ef6a6a6e-941e-43db-878c-4b51dffda250",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff9c7df0-3244-4849-b3e7-0ba8b1078e3e",
    "visible": true
}