{
    "id": "26aac89b-2112-4aa4-a6b0-4cfdfb026de1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEye",
    "eventList": [
        {
            "id": "a0bed245-2507-451e-981a-a60f2a016a71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26aac89b-2112-4aa4-a6b0-4cfdfb026de1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c43da00-b068-41b5-a2b0-01eaf0c87d5c",
    "visible": true
}