with(other) {
	if (invincible <= 0 && !dead) {
		shell.hp -= 1
		audio_play_sound(hitsound,2.5,false)

		if (shell.hp <= 0) {
			cDeath()
		} else {
			invincible = 40
		}
	}
}