{
    "id": "73ec79c6-df2d-4edb-8af2-76ed4cfe8895",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemy",
    "eventList": [
        {
            "id": "311926fc-6641-457c-afa5-325336e0aa2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ef49beae-6684-49a8-bec7-7f61f279e35e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "73ec79c6-df2d-4edb-8af2-76ed4cfe8895"
        },
        {
            "id": "5be2529d-1bec-4f71-960a-53dd4413abf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73ec79c6-df2d-4edb-8af2-76ed4cfe8895"
        },
        {
            "id": "6e04be35-5c15-4458-bf06-a7a7c6bc55a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73ec79c6-df2d-4edb-8af2-76ed4cfe8895"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "visible": true
}