{
    "id": "7bb10e34-c3fe-4944-9c45-f42a43f36311",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSkull",
    "eventList": [
        {
            "id": "120a3f51-76b0-4cf8-869a-bf6e3d659cc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7bb10e34-c3fe-4944-9c45-f42a43f36311"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f948a57f-736f-40f9-a1e3-157a38dc057e",
    "visible": true
}