{
    "id": "40ea4dad-ddc2-4b7c-8471-1d302e7df4f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLighter",
    "eventList": [
        {
            "id": "aadf6a28-750e-43f7-b772-7c6ac820e1c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40ea4dad-ddc2-4b7c-8471-1d302e7df4f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4999055a-0559-4111-9e39-f2d0d4d4ecb9",
    "visible": true
}