{
    "id": "c3f49585-3e26-40ca-a0d7-88141670fc28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAngryCrabLR",
    "eventList": [
        {
            "id": "34e0bb77-a283-4812-a1c4-3d8aea6ce542",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3f49585-3e26-40ca-a0d7-88141670fc28"
        }
    ],
    "maskSpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "overriddenProperties": null,
    "parentObjectId": "34e909f4-8b08-4faf-9226-a35ed710ea2d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bccbf3a1-3304-409d-8afa-b3ec02397b92",
    "visible": true
}