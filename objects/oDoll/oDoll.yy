{
    "id": "474abbf6-3171-48ec-b544-f1ff389f564e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoll",
    "eventList": [
        {
            "id": "b9c4f79c-54c5-4549-8e5f-d6a7d98517b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "474abbf6-3171-48ec-b544-f1ff389f564e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b984a364-e363-4520-ba23-ad8cc2c715fe",
    "visible": true
}