{
    "id": "1afde33b-58b8-43aa-92a9-6d9bba2104a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitle",
    "eventList": [
        {
            "id": "24b6a7e3-8c31-42f0-8d6b-d45364f88110",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "1afde33b-58b8-43aa-92a9-6d9bba2104a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3da4ab83-0aea-4e49-816d-4f3fbadc775d",
    "visible": true
}