{
    "id": "b2d7534d-44c8-495c-a1cf-e1356473d389",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRock",
    "eventList": [
        {
            "id": "eb7626fa-e2f3-411f-8364-7549a9999fe0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2d7534d-44c8-495c-a1cf-e1356473d389"
        }
    ],
    "maskSpriteId": "df2489ed-b320-417d-8f73-795d1450e8f6",
    "overriddenProperties": null,
    "parentObjectId": "c27580be-2c6a-49cd-a6e6-d232e2efcd8b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87ed7753-cbee-457f-8eed-38ce81fc5220",
    "visible": true
}