{
    "id": "4b4b9925-196b-4497-b171-6defa02a525e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStarfish",
    "eventList": [
        {
            "id": "18503b8a-9e28-4aeb-ad8b-d1b22f9324e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b4b9925-196b-4497-b171-6defa02a525e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "096e2a54-9a43-4a3b-a8f1-630aa70561d1",
    "visible": true
}