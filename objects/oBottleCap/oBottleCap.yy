{
    "id": "1de08dcb-b35f-42c3-92d3-82d7151ee33f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBottleCap",
    "eventList": [
        {
            "id": "470c0b99-c37f-4579-b075-b1670f50aadd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1de08dcb-b35f-42c3-92d3-82d7151ee33f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8594c873-6143-4dc5-8e3d-25d2c2ed427e",
    "visible": true
}