{
    "id": "7bd1c0c9-3296-4144-9601-2b375422e516",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTutorial",
    "eventList": [
        {
            "id": "a8f85271-53ce-4f63-bb61-c42fd5c23d06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7bd1c0c9-3296-4144-9601-2b375422e516"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c513d2ca-2566-45cd-9e2a-72bd98ef59f4",
    "visible": true
}