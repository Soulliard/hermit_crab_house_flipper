{
    "id": "df804ed5-e95e-4931-9ce0-1718278e450e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShinyA",
    "eventList": [
        {
            "id": "88c76195-a603-43c9-9623-7c1eb795e2ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df804ed5-e95e-4931-9ce0-1718278e450e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dfef6b58-e2f7-40bd-960d-6d12a6a8d5f5",
    "visible": true
}