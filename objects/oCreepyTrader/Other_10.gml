event_inherited()

dialogList = ds_list_create()

spookyCount = 0
for (i = 0; i < ds_list_size(oCrab.shell.shinies); i++){
	if (ds_list_find_value(oCrab.shell.shinies,i).isSpooky){
		spookyCount += 1
	}
}

if (oControl.shell2.traded) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I have the creepiest home ever!"
	ds_list_add(dialogList, dialog)
} else if (oCrab.shell != oControl.shell2) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I wish I had a home that creeped people out. Don't you?"
	ds_list_add(dialogList, dialog)
} else if (spookyCount < 3) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I wish I had a home that creeped people out, like yours."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I'd trade my home for it, but I'd like if you could make it"
	dialog.line2 = "a bit scarier first."
	ds_list_add(dialogList, dialog)
} else {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I wish I had a home that creeped people out, like yours."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Why don't we swap homes?"
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Thanks! I can't wait to creep people out."
	ds_list_add(dialogList, dialog)
	
	oControl.shell2.traded = true
	oCrab.shell = oControl.shell3
	shell = oControl.shell2
	shell.x = x
	shell.y = y + 18
	shell.depth = depth + 1
}

oControl.dialogList = dialogList
