{
    "id": "1257caf3-377a-42d3-827a-a589ba49f95d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCreepyTrader",
    "eventList": [
        {
            "id": "6ef2063b-66e3-43e2-a85b-b48532dcd9b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1257caf3-377a-42d3-827a-a589ba49f95d"
        },
        {
            "id": "1eb774fc-265d-4bb8-9ad9-d072333a7eb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1257caf3-377a-42d3-827a-a589ba49f95d"
        }
    ],
    "maskSpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "overriddenProperties": null,
    "parentObjectId": "2432e09a-5410-407e-8db6-2f2ad0984227",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78907ab9-0316-4aca-a5ff-57b3528662c0",
    "visible": true
}