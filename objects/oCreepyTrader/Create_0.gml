event_inherited()

if (oControl.shell2.traded) {
	shell = oControl.shell2
} else {
	shell = oControl.shell3	
}

shell.x = x
shell.y = y + 18
shell.depth = depth + 1
