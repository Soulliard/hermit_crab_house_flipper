{
    "id": "21201cfb-2426-4392-b48b-ef54cf311b01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlastic",
    "eventList": [
        {
            "id": "cc6d22d5-a8d2-4d3f-a0f7-fdf4b2c4ba7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21201cfb-2426-4392-b48b-ef54cf311b01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a360402-e74c-4052-84d4-14dc481ffdef",
    "visible": true
}