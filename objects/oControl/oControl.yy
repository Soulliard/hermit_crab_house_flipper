{
    "id": "066b6773-cf09-4103-ba8e-4cc65d233ed7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oControl",
    "eventList": [
        {
            "id": "69fd22c3-0518-4bb3-9a93-7f33a0f94c2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "066b6773-cf09-4103-ba8e-4cc65d233ed7"
        },
        {
            "id": "c56170a0-129b-4bcd-9a28-0d0bf91be1cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "066b6773-cf09-4103-ba8e-4cc65d233ed7"
        },
        {
            "id": "ed42fe3a-98d1-4aae-8c28-9275019b9364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "066b6773-cf09-4103-ba8e-4cc65d233ed7"
        },
        {
            "id": "f8b127ad-970b-4ac4-ab98-98969ba9eb47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "066b6773-cf09-4103-ba8e-4cc65d233ed7"
        },
        {
            "id": "30b61f92-9506-4655-a365-68c16f8277fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "066b6773-cf09-4103-ba8e-4cc65d233ed7"
        },
        {
            "id": "c4e56979-3966-4357-a726-12f7bd78c21d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "066b6773-cf09-4103-ba8e-4cc65d233ed7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}