if (instance_exists(oOldTrader)) {
	audio_stop_sound(bgloop)
	audio_play_sound(bgloopshiny,1,true)
} else if (!audio_is_playing(bgloop)) {
	audio_stop_sound(bgloopshiny)
	audio_play_sound(bgloop,1,true)
}
