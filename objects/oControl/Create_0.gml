pause = false
ending = false

mainFont = font_add_sprite(sFont, ord("!"), false, 0)
draw_set_font(mainFont)

depth = -10000
inDialog = false
dialogIndex = 0
canTalk = true

heardIntroSpeech = false

shell1 = instance_create_depth(-500, -500, 0, oShell1)
oCrab.shell = shell1
shell1.traded = false

shell2 = instance_create_depth(-500, -500, 0, oShell2)
shell2.traded = false

shell3 = instance_create_depth(-500, -500, 0, oShell3)
shell3.traded = false

shell4 = instance_create_depth(-500, -500, 0, oShell4)
shell4.traded = false

shell5 = instance_create_depth(-500, -500, 0, oShell5)
shell5.traded = false

audio_play_sound(bgloop,1,true)
