if (inDialog) {
	draw_set_color(c_black)
	
	text_x = view_xport[0]
	text_y = view_yport[0] + view_hport[0] - 100
	
	draw_rectangle(
		text_x, 
		text_y,
		view_xport[0] + view_wport[0],
		view_yport[0] + view_hport[0],
		false)
		
	dialog = ds_list_find_value(dialogList, dialogIndex)
	draw_set_color(c_white)
	
	draw_text(
		text_x + 5,
		text_y + 5,
		dialog.line1)
	draw_text(
		text_x + 5,
		text_y + 23,
		dialog.line2)
	draw_text(
		text_x + 5,
		text_y + 41,
		dialog.line3)
	draw_text(
		text_x + 5,
		text_y + 59,
		dialog.line4)
	draw_text(
		view_xport[0] + view_wport[0] - 117,
		text_y + 77,
		"Press Z")
}

heartIndex = 0
while (heartIndex < oCrab.shell.maxhp) {
	if (heartIndex < oCrab.shell.hp) {
		draw_sprite(sHealth, 0, view_xport[0] + 8 + 16*heartIndex, view_yport[0] + 8)	
	} else {
		draw_sprite(sHealthEmpty, 0, view_xport[0] + 8 + 16*heartIndex, view_yport[0] + 8)
	}
	heartIndex += 1
}
