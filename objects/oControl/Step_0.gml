if (inDialog && keyboard_check_pressed(ord("Z"))) {
	if (dialogIndex < ds_list_size(dialogList) - 1) {
		dialogIndex += 1	
		audio_play_sound(talksound,2,false)
	} else {
		inDialog = false
		pause = false
		alarm[0] = 2
		if (oCrab.dead) {
			game_restart()
		}
		if (ending) {
			game_restart()
		}
	}
}