{
    "id": "823f82da-8409-475c-9355-c570946aa363",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTrader",
    "eventList": [
        {
            "id": "53063fac-6108-4221-877f-af4886fd7346",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "823f82da-8409-475c-9355-c570946aa363"
        },
        {
            "id": "d8e2393a-ec6e-42fd-8066-d32ccf56a968",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "823f82da-8409-475c-9355-c570946aa363"
        }
    ],
    "maskSpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "overriddenProperties": null,
    "parentObjectId": "2432e09a-5410-407e-8db6-2f2ad0984227",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "622eab9c-756e-4029-8936-6530fdaa064c",
    "visible": true
}