event_inherited()

dialogList = ds_list_create()

garbageCount = 0
for (i = 0; i < ds_list_size(oCrab.shell.shinies); i++){
	if (ds_list_find_value(oCrab.shell.shinies,i).isTrash){
		garbageCount += 1
	}
}

if (oControl.shell1.traded) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I love my new home. It's so trashy!"
	ds_list_add(dialogList, dialog)
} else if (garbageCount < 3) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Hi there. I love your leather home! Humans leave the"
	dialog.line2 = "coolest things lying around."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I'd gladly trade my home for yours, if you added a few more"
	dialog.line2 = "bits of beautiful trash to it."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "3 pieces of trash would be perfect!"
	ds_list_add(dialogList, dialog)
} else {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Hi there. I love your leather home! Humans leave the"
	dialog.line2 = "coolest things lying around."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Can we trade homes? Yours is perfect!"
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Hooray! Thanks so much!"
	ds_list_add(dialogList, dialog)
	
	oControl.shell1.traded = true
	oCrab.shell = oControl.shell2
	shell = oControl.shell1
	shell.x = x
	shell.y = y + 18
	shell.depth = depth + 1
}

oControl.dialogList = dialogList
