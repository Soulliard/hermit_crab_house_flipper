{
    "id": "b224d39c-3ed4-4803-8926-ec9bdf6baee0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDagger",
    "eventList": [
        {
            "id": "10084553-9625-4463-8826-a040dfff1f03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b224d39c-3ed4-4803-8926-ec9bdf6baee0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ef5e277-717b-48b9-b1fc-aac1b5664837",
    "visible": true
}