{
    "id": "b0d4c027-86f9-4eba-9645-417db9a4278d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShell2",
    "eventList": [
        {
            "id": "76c6d80e-1f5e-422d-bd31-d64f57950e86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0d4c027-86f9-4eba-9645-417db9a4278d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ef6a6a6e-941e-43db-878c-4b51dffda250",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a2772199-f4dc-4677-8c05-a6a99e37078e",
    "visible": true
}