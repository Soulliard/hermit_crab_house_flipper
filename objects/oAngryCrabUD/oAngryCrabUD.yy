{
    "id": "34e909f4-8b08-4faf-9226-a35ed710ea2d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAngryCrabUD",
    "eventList": [
        {
            "id": "10ef0a22-ab7e-45b0-b9f6-a8b3ebe7d04c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34e909f4-8b08-4faf-9226-a35ed710ea2d"
        },
        {
            "id": "1dcbf95a-6ee0-4d0b-9f4a-919977a09189",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "34e909f4-8b08-4faf-9226-a35ed710ea2d"
        }
    ],
    "maskSpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "overriddenProperties": null,
    "parentObjectId": "73ec79c6-df2d-4edb-8af2-76ed4cfe8895",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bccbf3a1-3304-409d-8afa-b3ec02397b92",
    "visible": true
}