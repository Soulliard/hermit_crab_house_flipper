{
    "id": "3417688f-585c-447f-ae03-2e2168b0718f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oUrchinProjectile",
    "eventList": [
        
    ],
    "maskSpriteId": "ebc62856-a104-47fe-ac61-c0b2caff81a7",
    "overriddenProperties": null,
    "parentObjectId": "96cd2a82-a2ac-4ba7-b2c0-2d10201c6e9f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ce641023-6ea5-4a38-8c10-e79db6e83233",
    "visible": true
}