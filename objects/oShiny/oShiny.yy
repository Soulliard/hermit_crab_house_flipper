{
    "id": "423e6f47-6055-405d-b717-4a004f6e707f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShiny",
    "eventList": [
        {
            "id": "d882c482-26fd-4ea6-85ad-09e48dde5052",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "423e6f47-6055-405d-b717-4a004f6e707f"
        },
        {
            "id": "4b82085d-6c40-45ce-b4ba-3a6823e8dbad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "423e6f47-6055-405d-b717-4a004f6e707f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "317539e6-1025-445f-8f25-1692e0094f00",
    "visible": true
}