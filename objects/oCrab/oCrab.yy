{
    "id": "ef49beae-6684-49a8-bec7-7f61f279e35e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCrab",
    "eventList": [
        {
            "id": "9ad399e6-1cc5-4af4-981d-07f1a799f313",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ef49beae-6684-49a8-bec7-7f61f279e35e"
        },
        {
            "id": "c23b5aba-54b8-42cc-a764-17f6c8cc6abd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef49beae-6684-49a8-bec7-7f61f279e35e"
        },
        {
            "id": "fbf0dcc5-2929-45ad-91c5-965f1200b65e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ef49beae-6684-49a8-bec7-7f61f279e35e"
        }
    ],
    "maskSpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2370c04b-6887-4d13-ac99-a5488e8bbe91",
    "visible": true
}