if (!oControl.pause) {
	xspeed = 0
	yspeed = 0

	if (keyboard_check(vk_left)) {
		xspeed -= moveSpeed;
		alarm[0] = 0
	}
	if (keyboard_check(vk_right)) {
		xspeed += moveSpeed;
		alarm[0] = 0
	}
	if (keyboard_check(vk_up)) {
		yspeed -= moveSpeed;
		alarm[0] = 0
	}
	if (keyboard_check(vk_down)) {
		yspeed += moveSpeed;
		alarm[0] = 0
	}

	// Reduce diagonal speed.
	if (xspeed != 0 && yspeed != 0) {
		xspeed *= sqrt(2) / 2
		yspeed *= sqrt(2) / 2
	}
	
	if (xspeed != 0 || yspeed != 0) {
		sprite_index = sCrabWalk	
	} else {
		sprite_index = sCrab
	}

	cMove(xspeed, yspeed)
} else {
	sprite_index = sCrab	
}

shell.x = x
shell.y = y + 16

depth = -y
shell.depth = -y + 1

invincible -= 1

if (dead) {
	visible = true
	shell.visible = false
} else if (invincible > 0 && invincible mod 8 < 4) {
	visible = false
	shell.visible = false
} else {
	visible = true
	shell.visible = true
}

if (x > room_width) {
	if (oRoom.right != -1) {
		x = 16
		room_goto(oRoom.right)	
	}
}
if (x < 0) {
	if (oRoom.left != -1) {
		x = room_width - 16
		room_goto(oRoom.left)	
	}
}
if (y > room_height) {
	if (oRoom.down != -1) {
		y = 16
		room_goto(oRoom.down)	
	}
}
if (y < 0) {
	if (oRoom.up != -1) {
		y = room_height - 16
		room_goto(oRoom.up)	
	}
}
