{
    "id": "0a3a02b6-2c2b-4263-9da2-22ab93769001",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChain",
    "eventList": [
        {
            "id": "784d0b56-b6f7-4937-9583-4d2b87a08212",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a3a02b6-2c2b-4263-9da2-22ab93769001"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "423e6f47-6055-405d-b717-4a004f6e707f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ea6370d-dfb1-42e9-810b-11e9c5a77427",
    "visible": true
}