{
    "id": "96cd2a82-a2ac-4ba7-b2c0-2d10201c6e9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oProjectile",
    "eventList": [
        {
            "id": "c3843fb8-433e-4475-9eff-c8366e0219c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ef49beae-6684-49a8-bec7-7f61f279e35e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "96cd2a82-a2ac-4ba7-b2c0-2d10201c6e9f"
        },
        {
            "id": "a92d7a6a-4e76-4476-98ca-f6a38fc8e0db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "96cd2a82-a2ac-4ba7-b2c0-2d10201c6e9f"
        },
        {
            "id": "94b6cea0-8aca-4d6f-b8c5-163a7b61a318",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "96cd2a82-a2ac-4ba7-b2c0-2d10201c6e9f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73ec79c6-df2d-4edb-8af2-76ed4cfe8895",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ebc62856-a104-47fe-ac61-c0b2caff81a7",
    "visible": true
}