{
    "id": "2a0a99cf-48e6-4af4-9a82-ca2861161282",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oJones",
    "eventList": [
        {
            "id": "9c2d1503-ef4f-4cf3-a180-a85392dafc96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2a0a99cf-48e6-4af4-9a82-ca2861161282"
        },
        {
            "id": "12684d4f-eb65-404b-a24a-8fecf369f786",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a0a99cf-48e6-4af4-9a82-ca2861161282"
        },
        {
            "id": "4d0f3173-a71a-4d4e-8c45-3488646f47e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a0a99cf-48e6-4af4-9a82-ca2861161282"
        }
    ],
    "maskSpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "overriddenProperties": null,
    "parentObjectId": "2432e09a-5410-407e-8db6-2f2ad0984227",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8f4196b-8e65-48a0-ab37-77b6b93c1456",
    "visible": true
}