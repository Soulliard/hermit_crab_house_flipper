event_inherited()

dialogList = ds_list_create()

if (oCrab.shell == oControl.shell4) {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Your shell is... is..."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "It's the most beautiful thing I've ever seen."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "I'm sorry I made fun of you before. Material possessions"
	dialog.line2 = "aren't really that important anyways, right?"
	ds_list_add(dialogList, dialog)
	
	oControl.ending = true
} else {
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Your shell is pathetic."
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Not everyone can have a shell as perfect as mine."
	ds_list_add(dialogList, dialog)
}

oControl.dialogList = dialogList
