event_inherited()

if (!oControl.heardIntroSpeech && oCrab.x > x - 40) {
	oControl.pause = true
	oControl.inDialog = true
	oControl.dialogIndex = 0
	oControl.canTalk = false

	dialogList = ds_list_create()

	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "Hey, you! What is that on your back?"
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "You call that a home? It's literal garbage!"
	ds_list_add(dialogList, dialog)
	
	dialog = instance_create_depth(x, y, depth, oDialogBox)
	dialog.line1 = "You'll never have a shell as nice as mine."
	ds_list_add(dialogList, dialog)

	oControl.dialogList = dialogList

	oControl.heardIntroSpeech = true
	audio_play_sound(talksound,2,false)

}
