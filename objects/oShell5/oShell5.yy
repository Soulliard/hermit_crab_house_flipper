{
    "id": "8414418b-8dfe-45d5-a012-29784b9c5123",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShell5",
    "eventList": [
        {
            "id": "e25c665d-734b-4aa1-8dd0-0bb62067201d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8414418b-8dfe-45d5-a012-29784b9c5123"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ef6a6a6e-941e-43db-878c-4b51dffda250",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e949759-8f45-4431-add0-4d7e576ac38f",
    "visible": true
}