alarm[0] = 120

p = instance_create_depth(x, y, depth, oUrchinProjectile)
p.xspeed = 1.4
p.yspeed = 1.4
p.image_angle = 315

p = instance_create_depth(x, y, depth, oUrchinProjectile)
p.xspeed = 1.4
p.yspeed = -1.4
p.image_angle = 45

p = instance_create_depth(x, y, depth, oUrchinProjectile)
p.xspeed = -1.4
p.yspeed = -1.4
p.image_angle = 135

p = instance_create_depth(x, y, depth, oUrchinProjectile)
p.xspeed = -1.4
p.yspeed = 1.4
p.image_angle = 225
