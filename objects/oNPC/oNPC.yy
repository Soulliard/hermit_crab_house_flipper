{
    "id": "2432e09a-5410-407e-8db6-2f2ad0984227",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPC",
    "eventList": [
        {
            "id": "b950c28d-7c57-4ca3-a223-3209b5c82600",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2432e09a-5410-407e-8db6-2f2ad0984227"
        },
        {
            "id": "91be958e-16e3-4fba-9e30-7d4d50530002",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2432e09a-5410-407e-8db6-2f2ad0984227"
        },
        {
            "id": "6566cee3-13fa-40b2-bdff-56172e8d773f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2432e09a-5410-407e-8db6-2f2ad0984227"
        },
        {
            "id": "f9640a4d-3ef5-4e16-a044-311503b63bfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "2432e09a-5410-407e-8db6-2f2ad0984227"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}