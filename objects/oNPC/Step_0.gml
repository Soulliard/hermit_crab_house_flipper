depth = -y
shell.depth = depth + 1

talkPrompt.x = x
talkPrompt.y = y - 60
talkPrompt.depth = depth - 1000

if (place_meeting(x, y, oCrab) && !oControl.pause && oControl.canTalk) {
	if (keyboard_check_pressed(ord("Z"))) {
		talkPrompt.visible = false
		event_perform(ev_other, ev_user0)
	} else {
		talkPrompt.visible = true	
	}
} else {
	talkPrompt.visible = false
}
