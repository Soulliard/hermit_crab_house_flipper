{
    "id": "e9c11ef1-7408-4e45-9682-0c6228fb4c0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShell3",
    "eventList": [
        {
            "id": "7d5d9c2b-a7ba-4fea-8f57-06684270f2d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9c11ef1-7408-4e45-9682-0c6228fb4c0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ef6a6a6e-941e-43db-878c-4b51dffda250",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97bbff3a-04c8-4dd0-8ad5-2a0d0437e5ac",
    "visible": true
}