
{
    "name": "rRoomToCopy",
    "id": "b3eabee2-4358-45fa-8f32-5904946404cc",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "2e65338e-19be-4072-af13-f498af89ef2b",
        "9551b260-c008-4c81-835a-90fe29756565",
        "cf9c3281-28b6-45d1-a1ba-28556458bf12",
        "149fa34d-42ab-45f7-b916-8190f496d7bb",
        "0e92e2ce-9057-4d05-a232-52eecc752020",
        "f90a2670-46ea-424d-93bb-7bba681465b7",
        "b2e0b64a-0920-4370-8ddd-e096b8e13fc4",
        "14322ef8-bf8f-4cab-97f2-cd860b2fd7fa",
        "efeed290-668c-473e-99cc-5469788fc32b",
        "76e52057-fcb7-464c-a601-fdc29bbfd3fd",
        "57e897d6-b882-46bc-9cd7-cd4fe2b7fe39",
        "7d99ab08-f56e-4276-942a-6eb688a3d54f",
        "34671de1-144d-4ac6-ad04-0d654367b53d",
        "cea9b326-24b3-434d-ba66-481b38c9472e",
        "b5f54f3d-53ba-4985-a13b-e7621ccf941d",
        "e49d506e-6fc2-464c-b1bc-e66f609bb748",
        "a2e058f5-f337-46ea-92c7-e821b36a36cd",
        "048d7122-7556-4939-9b2e-678d51079e43",
        "182808e2-eba4-4787-8a99-23986400ba5b",
        "72d9de1f-9c1a-4fd8-8046-8a0aa325f0b1",
        "e13804bc-fd11-4f0d-a185-0d2792e37b30",
        "31b74e2f-cd86-433a-a58a-e385f90e83f5",
        "7be09d07-24df-4203-810c-cff3da609365",
        "7ef3bd95-e2b0-49d4-a98b-2f7d9a29707f",
        "e923d927-8b8b-47d4-b843-e575c76815db",
        "39638325-23e1-4313-871b-c95853c53475",
        "1f6200c3-afcb-47a2-bfdf-06ad5ba87ca8",
        "1038e6c2-aa8b-423a-9974-0fd47fb2871a",
        "e951da01-4708-485c-9c8b-4f0c80813469",
        "94c63382-76db-4eed-bed7-960872486955",
        "cc0d2b8b-b7ae-472a-a57a-9d3b4e5854c7",
        "c0f5c838-eb8c-4de1-8978-83bc77fb2267",
        "9e2d05d3-6d68-4967-ac92-8944a4bfbde0",
        "57f5d0fd-5f33-4b04-9c56-b98db22e361e",
        "9b1a415f-4e07-4348-aeaa-f2253d269af1",
        "bbf27afb-34f0-4102-bc82-8a1939caf687",
        "60ca9746-3436-4fd1-a5e7-daf96f23c2d5",
        "c748d257-ef46-48bc-bbe2-31469a5c7d8d",
        "556a1508-fba2-453c-9c3b-4df7e9e62cd1",
        "db9bf04f-97f2-439a-b928-3c34e1846bcf",
        "46f92104-7444-4f3b-bc39-9305deb00cfc",
        "49a8414d-d2df-4bfe-a06e-28c15b768576",
        "d271357b-a434-4a6c-ad09-96f5e8c96dec",
        "f93e3ecc-45b9-46c4-b101-647ed02fa939",
        "e3f38080-0c11-4e62-bfa3-aaccf4bb390a",
        "6277eba0-9a4a-4fcd-bd0d-33f1a7f2a18a",
        "887cea1b-f3df-4cd5-9e3f-a9a8be90dd0a",
        "1bf1fdfc-63b7-46ee-bf13-faa35744aaf4"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "52d47865-7b1f-4e2e-a280-7bd25e0870d1",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7C2AF09D","id": "2e65338e-19be-4072-af13-f498af89ef2b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7C2AF09D","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 928,"y": 0},
{"name": "inst_27CD2C7C","id": "9551b260-c008-4c81-835a-90fe29756565","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_27CD2C7C","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 896,"y": 0},
{"name": "inst_9A8A905","id": "cf9c3281-28b6-45d1-a1ba-28556458bf12","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_9A8A905","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 864,"y": 0},
{"name": "inst_3AD82E18","id": "149fa34d-42ab-45f7-b916-8190f496d7bb","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3AD82E18","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 832,"y": 0},
{"name": "inst_2A14174B","id": "0e92e2ce-9057-4d05-a232-52eecc752020","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2A14174B","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 800,"y": 0},
{"name": "inst_2EE170FB","id": "f90a2670-46ea-424d-93bb-7bba681465b7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2EE170FB","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 768,"y": 0},
{"name": "inst_8DE62BE","id": "b2e0b64a-0920-4370-8ddd-e096b8e13fc4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_8DE62BE","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 736,"y": 0},
{"name": "inst_23C8F31C","id": "14322ef8-bf8f-4cab-97f2-cd860b2fd7fa","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_23C8F31C","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 704,"y": 0},
{"name": "inst_632DB211","id": "efeed290-668c-473e-99cc-5469788fc32b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_632DB211","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 672,"y": 0},
{"name": "inst_5EDA5D45","id": "76e52057-fcb7-464c-a601-fdc29bbfd3fd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5EDA5D45","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 640,"y": 0},
{"name": "inst_32FB7FF1","id": "57e897d6-b882-46bc-9cd7-cd4fe2b7fe39","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_32FB7FF1","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 608,"y": 0},
{"name": "inst_5BFAA43C","id": "7d99ab08-f56e-4276-942a-6eb688a3d54f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5BFAA43C","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 576,"y": 0},
{"name": "inst_48F2B166","id": "34671de1-144d-4ac6-ad04-0d654367b53d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_48F2B166","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 352,"y": 0},
{"name": "inst_5925744B","id": "cea9b326-24b3-434d-ba66-481b38c9472e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5925744B","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 320,"y": 0},
{"name": "inst_1F8F9A56","id": "b5f54f3d-53ba-4985-a13b-e7621ccf941d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1F8F9A56","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 288,"y": 0},
{"name": "inst_60B432A1","id": "e49d506e-6fc2-464c-b1bc-e66f609bb748","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_60B432A1","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 256,"y": 0},
{"name": "inst_34B934E9","id": "a2e058f5-f337-46ea-92c7-e821b36a36cd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_34B934E9","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 0},
{"name": "inst_73923431","id": "048d7122-7556-4939-9b2e-678d51079e43","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_73923431","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 0},
{"name": "inst_4B58E040","id": "182808e2-eba4-4787-8a99-23986400ba5b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4B58E040","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 160,"y": 0},
{"name": "inst_5C03A3B3","id": "72d9de1f-9c1a-4fd8-8046-8a0aa325f0b1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5C03A3B3","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 128,"y": 0},
{"name": "inst_6D17A2C9","id": "e13804bc-fd11-4f0d-a185-0d2792e37b30","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6D17A2C9","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 0},
{"name": "inst_E61A45E","id": "31b74e2f-cd86-433a-a58a-e385f90e83f5","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_E61A45E","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 32,"y": 0},
{"name": "inst_1274C6BF","id": "7be09d07-24df-4203-810c-cff3da609365","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1274C6BF","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 0},
{"name": "inst_38204523","id": "7ef3bd95-e2b0-49d4-a98b-2f7d9a29707f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_38204523","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 0},
{"name": "inst_3EC03C9","id": "e923d927-8b8b-47d4-b843-e575c76815db","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3EC03C9","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 512},
{"name": "inst_66E2211D","id": "39638325-23e1-4313-871b-c95853c53475","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_66E2211D","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 32,"y": 512},
{"name": "inst_5990BBC","id": "1f6200c3-afcb-47a2-bfdf-06ad5ba87ca8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5990BBC","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 512},
{"name": "inst_2F8C097A","id": "1038e6c2-aa8b-423a-9974-0fd47fb2871a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2F8C097A","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 512},
{"name": "inst_2AC95C18","id": "e951da01-4708-485c-9c8b-4f0c80813469","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2AC95C18","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 128,"y": 512},
{"name": "inst_60A28459","id": "94c63382-76db-4eed-bed7-960872486955","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_60A28459","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 160,"y": 512},
{"name": "inst_4B707029","id": "cc0d2b8b-b7ae-472a-a57a-9d3b4e5854c7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4B707029","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 512},
{"name": "inst_3B336250","id": "c0f5c838-eb8c-4de1-8978-83bc77fb2267","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3B336250","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 512},
{"name": "inst_25CC2EA5","id": "9e2d05d3-6d68-4967-ac92-8944a4bfbde0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_25CC2EA5","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 256,"y": 512},
{"name": "inst_162D3E19","id": "57f5d0fd-5f33-4b04-9c56-b98db22e361e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_162D3E19","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 288,"y": 512},
{"name": "inst_7407CB42","id": "9b1a415f-4e07-4348-aeaa-f2253d269af1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7407CB42","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 320,"y": 512},
{"name": "inst_3A7864AD","id": "bbf27afb-34f0-4102-bc82-8a1939caf687","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3A7864AD","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 352,"y": 512},
{"name": "inst_415F2103","id": "60ca9746-3436-4fd1-a5e7-daf96f23c2d5","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_415F2103","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 576,"y": 512},
{"name": "inst_2EB89506","id": "c748d257-ef46-48bc-bbe2-31469a5c7d8d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2EB89506","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 608,"y": 512},
{"name": "inst_6BEC542A","id": "556a1508-fba2-453c-9c3b-4df7e9e62cd1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6BEC542A","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 640,"y": 512},
{"name": "inst_17EBE063","id": "db9bf04f-97f2-439a-b928-3c34e1846bcf","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_17EBE063","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 672,"y": 512},
{"name": "inst_31A64226","id": "46f92104-7444-4f3b-bc39-9305deb00cfc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_31A64226","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 704,"y": 512},
{"name": "inst_775BB48C","id": "49a8414d-d2df-4bfe-a06e-28c15b768576","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_775BB48C","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 736,"y": 512},
{"name": "inst_5B750BAE","id": "d271357b-a434-4a6c-ad09-96f5e8c96dec","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5B750BAE","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 768,"y": 512},
{"name": "inst_1B76BDFC","id": "f93e3ecc-45b9-46c4-b101-647ed02fa939","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1B76BDFC","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 800,"y": 512},
{"name": "inst_7313F792","id": "e3f38080-0c11-4e62-bfa3-aaccf4bb390a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7313F792","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 832,"y": 512},
{"name": "inst_161674D3","id": "6277eba0-9a4a-4fcd-bd0d-33f1a7f2a18a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_161674D3","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 864,"y": 512},
{"name": "inst_6F7F4DD7","id": "887cea1b-f3df-4cd5-9e3f-a9a8be90dd0a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6F7F4DD7","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 928,"y": 512},
{"name": "inst_4E107397","id": "1bf1fdfc-63b7-46ee-bf13-faa35744aaf4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4E107397","objId": "b2d7534d-44c8-495c-a1cf-e1356473d389","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 896,"y": 512}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "d5b07596-b03c-4b5d-9b22-29255f55e5ac",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4287680744 },
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "1e894441-3158-49bf-9339-3c69f7d2a681",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "ac2183b9-29c8-4976-aaf3-bd127e6def66",
        "Height": 540,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 960
    },
    "mvc": "1.0",
    "views": [
{"id": "cb97fbd6-7f9a-4320-ad30-76538455ce22","hborder": 32,"hport": 540,"hspeed": -1,"hview": 540,"inherit": false,"modelName": "GMRView","objId": "ef49beae-6684-49a8-bec7-7f61f279e35e","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 960,"wview": 960,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1a6be73a-6356-4e8a-92f1-0ffda27096d7","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f3865017-5c78-4e10-a08f-8ffb32585995","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "fe0ea8e1-ee09-475e-a4c4-ce2a0a6496f0","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "3dac7374-d58e-4d14-baad-441ca925f049","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "0b5c6a1d-e876-48d2-8157-ce9c9987b475","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "667574e0-807c-46ba-86ec-0413b92ba0d8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e0023c8b-bc12-40bf-a725-963f855cb2e0","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "e2951f81-9c1d-4334-962a-2c7b7fa978de",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}