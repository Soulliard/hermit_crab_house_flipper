{
    "id": "622eab9c-756e-4029-8936-6530fdaa064c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrader",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "786ca30e-8383-4391-bd20-0d19fa2d44df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622eab9c-756e-4029-8936-6530fdaa064c",
            "compositeImage": {
                "id": "fc112b08-f969-48c2-95b0-4f08e1ffec30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "786ca30e-8383-4391-bd20-0d19fa2d44df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f18c4d54-bc68-49db-9956-c73ccef02ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "786ca30e-8383-4391-bd20-0d19fa2d44df",
                    "LayerId": "6d20f579-313e-4bce-96b4-81855227fac2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "6d20f579-313e-4bce-96b4-81855227fac2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622eab9c-756e-4029-8936-6530fdaa064c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 17
}