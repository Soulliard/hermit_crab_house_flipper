{
    "id": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrabMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b41d0292-175d-415e-aad4-16b50c5def88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
            "compositeImage": {
                "id": "11b1b8f8-4c00-4b25-a1b8-aea5f916f7b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41d0292-175d-415e-aad4-16b50c5def88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b78306-42c1-4dee-a003-6ab181b0cc85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41d0292-175d-415e-aad4-16b50c5def88",
                    "LayerId": "9cffde67-1763-4b40-91e9-7987afa889e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "9cffde67-1763-4b40-91e9-7987afa889e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a6e7ed3-16b1-4cb2-a533-c93bb5aa1ec2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}