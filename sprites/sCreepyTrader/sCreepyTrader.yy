{
    "id": "78907ab9-0316-4aca-a5ff-57b3528662c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCreepyTrader",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29c68297-0549-4bca-bfb1-046e20c4c855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78907ab9-0316-4aca-a5ff-57b3528662c0",
            "compositeImage": {
                "id": "526f103f-0ea8-4372-a5c0-9b4f116ee6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c68297-0549-4bca-bfb1-046e20c4c855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1a16234-a551-4cae-a063-85784c57cd2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c68297-0549-4bca-bfb1-046e20c4c855",
                    "LayerId": "8a25aed9-e2a4-464f-b9a2-9e26b7d2e4dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "8a25aed9-e2a4-464f-b9a2-9e26b7d2e4dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78907ab9-0316-4aca-a5ff-57b3528662c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 17
}