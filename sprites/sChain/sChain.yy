{
    "id": "5ea6370d-dfb1-42e9-810b-11e9c5a77427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b955082-742d-4cdb-a55a-432fbfdc7b3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ea6370d-dfb1-42e9-810b-11e9c5a77427",
            "compositeImage": {
                "id": "68712fc9-a7da-41de-82f4-f55827d506cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b955082-742d-4cdb-a55a-432fbfdc7b3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3583c68f-277b-4242-93ae-84a6d7f1e06f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b955082-742d-4cdb-a55a-432fbfdc7b3d",
                    "LayerId": "505ad58c-5486-4332-8046-8e39de49a56d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "505ad58c-5486-4332-8046-8e39de49a56d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ea6370d-dfb1-42e9-810b-11e9c5a77427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 0
}