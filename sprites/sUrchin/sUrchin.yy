{
    "id": "6562099b-ecf7-4281-ab0a-5624eb2fa832",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUrchin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9ac7b67-7e59-412c-a13f-2231a0d4d49f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6562099b-ecf7-4281-ab0a-5624eb2fa832",
            "compositeImage": {
                "id": "4a30d1d8-e20a-4759-82d3-775cb59ff283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9ac7b67-7e59-412c-a13f-2231a0d4d49f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68c088fc-b743-4219-b669-897704150a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9ac7b67-7e59-412c-a13f-2231a0d4d49f",
                    "LayerId": "dbdbf5d6-b7db-4ab7-ae25-144a0e077feb"
                }
            ]
        },
        {
            "id": "38dfc7e0-2521-48fe-a2f2-c593030da612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6562099b-ecf7-4281-ab0a-5624eb2fa832",
            "compositeImage": {
                "id": "8058e0f2-f63c-483c-bf9e-7575993c5a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38dfc7e0-2521-48fe-a2f2-c593030da612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80ae0fe5-f2aa-4cf9-9a0f-7a919b830d2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38dfc7e0-2521-48fe-a2f2-c593030da612",
                    "LayerId": "dbdbf5d6-b7db-4ab7-ae25-144a0e077feb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dbdbf5d6-b7db-4ab7-ae25-144a0e077feb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6562099b-ecf7-4281-ab0a-5624eb2fa832",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}