{
    "id": "bccbf3a1-3304-409d-8afa-b3ec02397b92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAngryCrab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbafe56a-914f-4322-80ce-6891f07dcaa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bccbf3a1-3304-409d-8afa-b3ec02397b92",
            "compositeImage": {
                "id": "2634b79f-4e68-48b0-85d0-02adf0a3d453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbafe56a-914f-4322-80ce-6891f07dcaa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066b68fa-55aa-4d26-ba5e-3b2be15fdfe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbafe56a-914f-4322-80ce-6891f07dcaa0",
                    "LayerId": "5700fa0d-d9fe-4c39-9dd5-c1628d4de605"
                }
            ]
        },
        {
            "id": "8881dfcf-7bec-468f-be75-3bcd9e578b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bccbf3a1-3304-409d-8afa-b3ec02397b92",
            "compositeImage": {
                "id": "8556266e-d992-4772-add6-0fe07e527dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8881dfcf-7bec-468f-be75-3bcd9e578b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9e7446-4516-4f2b-9bb2-455c20b894d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8881dfcf-7bec-468f-be75-3bcd9e578b48",
                    "LayerId": "5700fa0d-d9fe-4c39-9dd5-c1628d4de605"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5700fa0d-d9fe-4c39-9dd5-c1628d4de605",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bccbf3a1-3304-409d-8afa-b3ec02397b92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}