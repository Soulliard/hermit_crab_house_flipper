{
    "id": "6e461e87-1168-479f-9a59-e28975cb4841",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOEye",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 45,
    "bbox_right": 64,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c26110f6-f095-4968-8894-29169a60e23b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e461e87-1168-479f-9a59-e28975cb4841",
            "compositeImage": {
                "id": "fe99ab10-0d31-4f19-b187-e8d3335f41e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c26110f6-f095-4968-8894-29169a60e23b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4bef4b7-d6bb-4022-86eb-9c52ef60e4d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c26110f6-f095-4968-8894-29169a60e23b",
                    "LayerId": "53829e8a-7a7a-423d-831a-22a09a084fd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "53829e8a-7a7a-423d-831a-22a09a084fd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e461e87-1168-479f-9a59-e28975cb4841",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}