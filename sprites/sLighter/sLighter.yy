{
    "id": "4999055a-0559-4111-9e39-f2d0d4d4ecb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLighter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01a7ddea-df15-459f-a191-022c6aa64810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4999055a-0559-4111-9e39-f2d0d4d4ecb9",
            "compositeImage": {
                "id": "88a84060-8d2e-4339-939b-d0c6a759622f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01a7ddea-df15-459f-a191-022c6aa64810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843c54aa-7736-4bb9-a524-2251031046e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01a7ddea-df15-459f-a191-022c6aa64810",
                    "LayerId": "76ddf240-dc0b-4061-9229-489d0e73f53e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "76ddf240-dc0b-4061-9229-489d0e73f53e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4999055a-0559-4111-9e39-f2d0d4d4ecb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}