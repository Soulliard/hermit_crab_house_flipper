{
    "id": "f948a57f-736f-40f9-a1e3-157a38dc057e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSkull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99670e80-2f1e-486f-af83-83be89168d44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f948a57f-736f-40f9-a1e3-157a38dc057e",
            "compositeImage": {
                "id": "0960dc4b-3976-4cb4-87b7-15e01a3ba8c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99670e80-2f1e-486f-af83-83be89168d44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a78329f2-0a8d-4882-a03e-64b277d033d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99670e80-2f1e-486f-af83-83be89168d44",
                    "LayerId": "2f7ca1d2-2969-4970-b065-ecbcba57cd4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2f7ca1d2-2969-4970-b065-ecbcba57cd4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f948a57f-736f-40f9-a1e3-157a38dc057e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 0,
    "yorig": 0
}