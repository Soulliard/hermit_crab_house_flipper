{
    "id": "97bbff3a-04c8-4dd0-8ad5-2a0d0437e5ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShell3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 9,
    "bbox_right": 98,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ef9796f-50c9-4220-b8e4-68f5d1861575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97bbff3a-04c8-4dd0-8ad5-2a0d0437e5ac",
            "compositeImage": {
                "id": "ae505dcc-dec1-4239-aabb-b7d5b53e7888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef9796f-50c9-4220-b8e4-68f5d1861575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc67ccc-7ea2-4951-a287-060a93ca513b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef9796f-50c9-4220-b8e4-68f5d1861575",
                    "LayerId": "1a3225aa-95a2-45a8-a00e-2140b1464fcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "1a3225aa-95a2-45a8-a00e-2140b1464fcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97bbff3a-04c8-4dd0-8ad5-2a0d0437e5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}