{
    "id": "6c43da00-b068-41b5-a2b0-01eaf0c87d5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEye",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eadf4da7-d2eb-4862-a667-95ded5bf51a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c43da00-b068-41b5-a2b0-01eaf0c87d5c",
            "compositeImage": {
                "id": "3af62f94-2aed-4120-98fe-ba0bc4cf71ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eadf4da7-d2eb-4862-a667-95ded5bf51a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e8df9f9-881f-442f-9eeb-bc8c7e8dd876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eadf4da7-d2eb-4862-a667-95ded5bf51a2",
                    "LayerId": "8d1e494f-4ddc-4bcf-95f4-5b0156d9db39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "8d1e494f-4ddc-4bcf-95f4-5b0156d9db39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c43da00-b068-41b5-a2b0-01eaf0c87d5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 0,
    "yorig": 0
}