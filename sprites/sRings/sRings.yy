{
    "id": "1a360402-e74c-4052-84d4-14dc481ffdef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5904a8c-35df-49e2-a50e-1271426fdc85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a360402-e74c-4052-84d4-14dc481ffdef",
            "compositeImage": {
                "id": "18f9bd10-80b4-4d7c-9066-6bdc7266bbf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5904a8c-35df-49e2-a50e-1271426fdc85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782b966d-c43b-4351-8ce7-7bf1c888ca83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5904a8c-35df-49e2-a50e-1271426fdc85",
                    "LayerId": "2cf5ff85-bf28-480b-92da-86056e37f00f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "2cf5ff85-bf28-480b-92da-86056e37f00f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a360402-e74c-4052-84d4-14dc481ffdef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": -25,
    "yorig": 21
}