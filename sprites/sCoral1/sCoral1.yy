{
    "id": "0cc42d82-f2b1-42c8-8347-0076c1b7f67e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCoral1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff4d6d2c-b875-4595-b516-09ec802687da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cc42d82-f2b1-42c8-8347-0076c1b7f67e",
            "compositeImage": {
                "id": "ac976bba-7789-4e62-911f-7e32c4a5dd7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff4d6d2c-b875-4595-b516-09ec802687da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5c2600b-ac03-4d6b-9805-f65597abc86d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff4d6d2c-b875-4595-b516-09ec802687da",
                    "LayerId": "e6c89c83-ca07-445e-be59-2a09dc57e098"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "e6c89c83-ca07-445e-be59-2a09dc57e098",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cc42d82-f2b1-42c8-8347-0076c1b7f67e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 51,
    "xorig": 0,
    "yorig": 0
}