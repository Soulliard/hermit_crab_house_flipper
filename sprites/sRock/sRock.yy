{
    "id": "87ed7753-cbee-457f-8eed-38ce81fc5220",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69fa4e1c-d6fb-470f-b4e3-8209001721dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87ed7753-cbee-457f-8eed-38ce81fc5220",
            "compositeImage": {
                "id": "e9ceb418-9c73-478c-9a54-74870f7667f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69fa4e1c-d6fb-470f-b4e3-8209001721dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eecd39b0-923d-4758-b0bf-fa0fd5ab808e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69fa4e1c-d6fb-470f-b4e3-8209001721dd",
                    "LayerId": "d13144eb-63ea-4573-8146-6e2d7c19840d"
                }
            ]
        },
        {
            "id": "8e814c23-4a57-43df-84d0-217b48e294fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87ed7753-cbee-457f-8eed-38ce81fc5220",
            "compositeImage": {
                "id": "a8cc808a-c881-40b5-80d3-a4fd4977525f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e814c23-4a57-43df-84d0-217b48e294fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7bf37a-5129-466c-9a6f-e0408d899ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e814c23-4a57-43df-84d0-217b48e294fd",
                    "LayerId": "d13144eb-63ea-4573-8146-6e2d7c19840d"
                }
            ]
        },
        {
            "id": "dcd7ea59-86a3-4863-b019-85f5f3c0718f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87ed7753-cbee-457f-8eed-38ce81fc5220",
            "compositeImage": {
                "id": "2ff4bd97-e54b-42dd-907a-ce9900927497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcd7ea59-86a3-4863-b019-85f5f3c0718f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e65428-909a-4b0b-8be6-ef23c7455d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcd7ea59-86a3-4863-b019-85f5f3c0718f",
                    "LayerId": "d13144eb-63ea-4573-8146-6e2d7c19840d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "d13144eb-63ea-4573-8146-6e2d7c19840d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87ed7753-cbee-457f-8eed-38ce81fc5220",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 4,
    "yorig": 4
}