{
    "id": "e64421ac-7b9f-459e-a25f-b3200267352e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOCoral1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 6,
    "bbox_right": 53,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "accd998c-dbd7-4204-a7e1-43a8a63dbccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64421ac-7b9f-459e-a25f-b3200267352e",
            "compositeImage": {
                "id": "b5902fc8-9c0c-461d-a000-fd7deab40e7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "accd998c-dbd7-4204-a7e1-43a8a63dbccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc23ac5-cbb0-41d7-8842-5c856e3c83fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "accd998c-dbd7-4204-a7e1-43a8a63dbccc",
                    "LayerId": "859c9d92-3b55-41f0-995b-56ea6f9b0ab3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "859c9d92-3b55-41f0-995b-56ea6f9b0ab3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e64421ac-7b9f-459e-a25f-b3200267352e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}