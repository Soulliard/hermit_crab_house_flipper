{
    "id": "2fe2c897-9c39-4dfd-b134-f4c394494d13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOCoral2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 29,
    "bbox_right": 61,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cba1f594-1c29-40c8-960e-7dad06105f4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fe2c897-9c39-4dfd-b134-f4c394494d13",
            "compositeImage": {
                "id": "afaabd2c-6bbe-49e8-ad0c-bcfce02df56a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba1f594-1c29-40c8-960e-7dad06105f4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a98ff24-8d10-453e-87bb-56cbe3ab3606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba1f594-1c29-40c8-960e-7dad06105f4f",
                    "LayerId": "4ebbe8cb-be93-4c62-9483-c12cf7fc835f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "4ebbe8cb-be93-4c62-9483-c12cf7fc835f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fe2c897-9c39-4dfd-b134-f4c394494d13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}