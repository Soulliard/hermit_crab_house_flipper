{
    "id": "df2489ed-b320-417d-8f73-795d1450e8f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc4d6df7-4373-41bb-a7c0-eafafcf155be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2489ed-b320-417d-8f73-795d1450e8f6",
            "compositeImage": {
                "id": "4a6d408c-e99e-45c1-8375-570d90d787a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc4d6df7-4373-41bb-a7c0-eafafcf155be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d55e7ae-15db-4c79-a92a-55d0bcc1aa2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc4d6df7-4373-41bb-a7c0-eafafcf155be",
                    "LayerId": "a3e425ee-34e8-41de-9e8b-4bac13d00c25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a3e425ee-34e8-41de-9e8b-4bac13d00c25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df2489ed-b320-417d-8f73-795d1450e8f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}