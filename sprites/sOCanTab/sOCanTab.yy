{
    "id": "4eeb6ddd-4b3b-48c5-ad37-3199b2a0b7da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOCanTab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 14,
    "bbox_right": 35,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8271992-ae20-41c6-bab0-3cb99acc086a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4eeb6ddd-4b3b-48c5-ad37-3199b2a0b7da",
            "compositeImage": {
                "id": "0fb88bc1-7c65-4c03-96db-bc224aafee0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8271992-ae20-41c6-bab0-3cb99acc086a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37f08977-a057-4035-9315-7cdfff6f6b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8271992-ae20-41c6-bab0-3cb99acc086a",
                    "LayerId": "0d9938b0-08b6-44ae-8f88-c3e14173b911"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "0d9938b0-08b6-44ae-8f88-c3e14173b911",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4eeb6ddd-4b3b-48c5-ad37-3199b2a0b7da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}