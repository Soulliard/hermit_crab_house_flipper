{
    "id": "a2772199-f4dc-4677-8c05-a6a99e37078e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShell2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 23,
    "bbox_right": 98,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b71a5c2-e7b3-4435-a505-5ef42ed012ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2772199-f4dc-4677-8c05-a6a99e37078e",
            "compositeImage": {
                "id": "3fa68330-f094-4021-965b-2068cb9a35cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b71a5c2-e7b3-4435-a505-5ef42ed012ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7489453-039d-47a3-8cce-7c0de0114f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b71a5c2-e7b3-4435-a505-5ef42ed012ba",
                    "LayerId": "0ab22ff6-7416-47a2-9c52-3d449834a9b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "0ab22ff6-7416-47a2-9c52-3d449834a9b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2772199-f4dc-4677-8c05-a6a99e37078e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}