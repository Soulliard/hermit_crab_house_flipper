{
    "id": "0b155a49-d00e-416a-9917-67ae7081751d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fakesprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87357f66-6b76-4089-909e-2bcdcb53f7ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b155a49-d00e-416a-9917-67ae7081751d",
            "compositeImage": {
                "id": "cb03201d-493f-4e46-beed-504ffd0e8180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87357f66-6b76-4089-909e-2bcdcb53f7ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02facf6e-4103-45f7-90de-9a6a3173e84d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87357f66-6b76-4089-909e-2bcdcb53f7ac",
                    "LayerId": "a58e63e3-3064-4822-a874-ccbb7a840008"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a58e63e3-3064-4822-a874-ccbb7a840008",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b155a49-d00e-416a-9917-67ae7081751d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}