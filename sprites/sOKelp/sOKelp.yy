{
    "id": "cc601e1e-8115-49d2-bc04-47f9c2644ddd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOKelp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 46,
    "bbox_right": 87,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6edfb803-3068-4a5e-a801-d02b9ff24d01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc601e1e-8115-49d2-bc04-47f9c2644ddd",
            "compositeImage": {
                "id": "051a4cf4-ef3f-4083-8615-e9499cbc9119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6edfb803-3068-4a5e-a801-d02b9ff24d01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d149c4ed-0815-406c-ac10-363ee4395258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6edfb803-3068-4a5e-a801-d02b9ff24d01",
                    "LayerId": "61714f45-11ac-46b5-bb15-84077e492411"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "61714f45-11ac-46b5-bb15-84077e492411",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc601e1e-8115-49d2-bc04-47f9c2644ddd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}