{
    "id": "4ef5e277-717b-48b9-b1fc-aac1b5664837",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDagger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4b5c1f8-5412-4841-b5f0-f40b155c7d17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef5e277-717b-48b9-b1fc-aac1b5664837",
            "compositeImage": {
                "id": "87281983-e0d5-41b0-906f-8e67bee334e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b5c1f8-5412-4841-b5f0-f40b155c7d17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccdfe4d6-7392-41c8-a35e-f880a2e78264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b5c1f8-5412-4841-b5f0-f40b155c7d17",
                    "LayerId": "520756b8-cceb-4fd9-a082-37b012724bd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "520756b8-cceb-4fd9-a082-37b012724bd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ef5e277-717b-48b9-b1fc-aac1b5664837",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 0,
    "yorig": 0
}