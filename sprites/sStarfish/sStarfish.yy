{
    "id": "096e2a54-9a43-4a3b-a8f1-630aa70561d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStarfish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ecb8d92-04e4-4da0-9cb8-e6d9d99a0ef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "096e2a54-9a43-4a3b-a8f1-630aa70561d1",
            "compositeImage": {
                "id": "ef8dd842-58c1-4001-af31-3e113c2614e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ecb8d92-04e4-4da0-9cb8-e6d9d99a0ef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2affd76-93f5-4f3e-bf91-7d4178df33d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ecb8d92-04e4-4da0-9cb8-e6d9d99a0ef7",
                    "LayerId": "7fb18d6c-bceb-4a31-a156-b574d102420d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "7fb18d6c-bceb-4a31-a156-b574d102420d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "096e2a54-9a43-4a3b-a8f1-630aa70561d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 51,
    "xorig": 0,
    "yorig": 0
}