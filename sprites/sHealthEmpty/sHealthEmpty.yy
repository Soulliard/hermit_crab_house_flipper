{
    "id": "d8b7f3fc-3487-4723-aa6e-87d0bb6746bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHealthEmpty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e50308b1-49b5-40cb-9bae-859b1fb32ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8b7f3fc-3487-4723-aa6e-87d0bb6746bf",
            "compositeImage": {
                "id": "e8d429d2-5e00-42e2-a780-29e9d1efab0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e50308b1-49b5-40cb-9bae-859b1fb32ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ece883-58d5-468f-a6b0-174c45bd7239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e50308b1-49b5-40cb-9bae-859b1fb32ce8",
                    "LayerId": "97cbefc6-6b72-4152-9e74-a0719fd657f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "97cbefc6-6b72-4152-9e74-a0719fd657f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8b7f3fc-3487-4723-aa6e-87d0bb6746bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}