{
    "id": "3787ef8b-85e2-4f1c-85c4-b4de9e2261e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOSkull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 26,
    "bbox_right": 75,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3cd4a48-40b4-4037-b3de-dff7aa9bad38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3787ef8b-85e2-4f1c-85c4-b4de9e2261e3",
            "compositeImage": {
                "id": "e504c186-c925-4f93-a045-8f294746c367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3cd4a48-40b4-4037-b3de-dff7aa9bad38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbeadf05-2138-4f03-ab21-01f8edd6b143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3cd4a48-40b4-4037-b3de-dff7aa9bad38",
                    "LayerId": "53b65c5e-3064-4517-8e4f-1512857f47c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "53b65c5e-3064-4517-8e4f-1512857f47c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3787ef8b-85e2-4f1c-85c4-b4de9e2261e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}