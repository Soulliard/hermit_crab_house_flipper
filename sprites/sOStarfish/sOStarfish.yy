{
    "id": "15932fe9-fce4-46be-af8d-9af435215561",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOStarfish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 26,
    "bbox_right": 74,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c250ba2f-58dc-4910-8cbd-75c78587d906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15932fe9-fce4-46be-af8d-9af435215561",
            "compositeImage": {
                "id": "0100433d-3e62-4e33-9f92-bc87ee14cce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c250ba2f-58dc-4910-8cbd-75c78587d906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12509383-b7a8-4bf5-b526-6d6edcdedc9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c250ba2f-58dc-4910-8cbd-75c78587d906",
                    "LayerId": "9be494ad-7d76-4af4-9b5e-cdd30a05eeec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "9be494ad-7d76-4af4-9b5e-cdd30a05eeec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15932fe9-fce4-46be-af8d-9af435215561",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}