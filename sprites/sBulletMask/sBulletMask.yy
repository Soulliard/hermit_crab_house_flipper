{
    "id": "ebc62856-a104-47fe-ac61-c0b2caff81a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBulletMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc3ffc7f-de81-4c3c-9177-e674cb5a0c2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebc62856-a104-47fe-ac61-c0b2caff81a7",
            "compositeImage": {
                "id": "10d5d9e8-2cfa-4663-9b15-4b02c2eae84b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc3ffc7f-de81-4c3c-9177-e674cb5a0c2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b4d454-1bdf-4d93-b931-8d3b7c5351f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc3ffc7f-de81-4c3c-9177-e674cb5a0c2c",
                    "LayerId": "69cb4b95-9e2b-4827-963f-bd8d19a18470"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "69cb4b95-9e2b-4827-963f-bd8d19a18470",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebc62856-a104-47fe-ac61-c0b2caff81a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}