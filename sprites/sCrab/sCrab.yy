{
    "id": "2370c04b-6887-4d13-ac99-a5488e8bbe91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08b038a9-fb95-4161-ad3c-4065f645a31d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2370c04b-6887-4d13-ac99-a5488e8bbe91",
            "compositeImage": {
                "id": "5fa3e2d4-a187-4090-a49b-d271c384e8fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08b038a9-fb95-4161-ad3c-4065f645a31d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367954bb-66d4-417b-a116-a3dd14634ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08b038a9-fb95-4161-ad3c-4065f645a31d",
                    "LayerId": "dcbde96a-9ac6-45ac-842e-c957564f5c00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "dcbde96a-9ac6-45ac-842e-c957564f5c00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2370c04b-6887-4d13-ac99-a5488e8bbe91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 17
}