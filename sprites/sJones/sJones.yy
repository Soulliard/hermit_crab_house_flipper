{
    "id": "b8f4196b-8e65-48a0-ab37-77b6b93c1456",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJones",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ccb2fd7-446a-4bbe-a1ef-57aa084d135f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8f4196b-8e65-48a0-ab37-77b6b93c1456",
            "compositeImage": {
                "id": "a05f3c17-58a2-444d-81e2-142ddcd52d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ccb2fd7-446a-4bbe-a1ef-57aa084d135f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18e21d38-bf5d-4070-ad54-d8c35ee8f652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ccb2fd7-446a-4bbe-a1ef-57aa084d135f",
                    "LayerId": "60f6d920-b927-458a-960e-ece6f9c4fbfa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "60f6d920-b927-458a-960e-ece6f9c4fbfa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8f4196b-8e65-48a0-ab37-77b6b93c1456",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 17
}