{
    "id": "03d626fe-4e80-4f8d-9b4f-9e0e4e027bc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sODagger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 5,
    "bbox_right": 53,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae1c0363-3b15-4973-97d1-d19295f2ce7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03d626fe-4e80-4f8d-9b4f-9e0e4e027bc1",
            "compositeImage": {
                "id": "dd102dc9-18c1-42c7-bae8-88ffb82e642b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae1c0363-3b15-4973-97d1-d19295f2ce7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58393894-9334-4d5e-9a6f-a164516ccf15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae1c0363-3b15-4973-97d1-d19295f2ce7b",
                    "LayerId": "ddd83f48-b2d5-47a4-b9e8-c790fe1ff9ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "ddd83f48-b2d5-47a4-b9e8-c790fe1ff9ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03d626fe-4e80-4f8d-9b4f-9e0e4e027bc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}