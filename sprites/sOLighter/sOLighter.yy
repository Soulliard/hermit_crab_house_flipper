{
    "id": "1be1023b-139c-4897-8fa3-403211db6be0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOLighter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 31,
    "bbox_right": 46,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec218b06-65bf-4701-8c39-46b560afe1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be1023b-139c-4897-8fa3-403211db6be0",
            "compositeImage": {
                "id": "ffc187d5-fc54-46ab-9c85-44bad5f52cc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec218b06-65bf-4701-8c39-46b560afe1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38f2aae-8656-4c34-a62a-77dba5ac762e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec218b06-65bf-4701-8c39-46b560afe1a3",
                    "LayerId": "bb4112b6-951d-4eb1-a79e-b63274f4cd32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "bb4112b6-951d-4eb1-a79e-b63274f4cd32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1be1023b-139c-4897-8fa3-403211db6be0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}