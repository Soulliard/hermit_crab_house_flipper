{
    "id": "b984a364-e363-4520-ba23-ad8cc2c715fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30789c30-cb32-4e96-bfa1-8ec198e1edea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b984a364-e363-4520-ba23-ad8cc2c715fe",
            "compositeImage": {
                "id": "5c58187c-c7aa-4808-9bfd-ab1ecf894c9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30789c30-cb32-4e96-bfa1-8ec198e1edea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7adff8e7-ba32-46c3-8a7f-99d3b309e8c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30789c30-cb32-4e96-bfa1-8ec198e1edea",
                    "LayerId": "8e59357f-be5c-4aab-a902-5c6f94642835"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "8e59357f-be5c-4aab-a902-5c6f94642835",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b984a364-e363-4520-ba23-ad8cc2c715fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 0,
    "yorig": 0
}