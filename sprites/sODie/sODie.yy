{
    "id": "66dbf54d-d1d5-4474-b3f0-7a4dc0da407e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sODie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 61,
    "bbox_right": 77,
    "bbox_top": 76,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0eea66d9-c721-4954-a1c4-cd451040d651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66dbf54d-d1d5-4474-b3f0-7a4dc0da407e",
            "compositeImage": {
                "id": "86fdbeec-c336-4258-a66e-8ad554a79e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eea66d9-c721-4954-a1c4-cd451040d651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64315f1-1335-40ff-b233-247b7a159ea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eea66d9-c721-4954-a1c4-cd451040d651",
                    "LayerId": "981f2098-7e74-4a9a-9183-c31870004e5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "981f2098-7e74-4a9a-9183-c31870004e5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66dbf54d-d1d5-4474-b3f0-7a4dc0da407e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}