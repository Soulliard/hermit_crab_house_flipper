{
    "id": "27f74558-22a4-43ef-b09f-2f3e105cde3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOldTrader",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a847c9a9-b094-4cee-bdfb-2e7b0ac28dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f74558-22a4-43ef-b09f-2f3e105cde3e",
            "compositeImage": {
                "id": "91cc6cd0-0662-4e0e-8778-6c4dbc6667f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a847c9a9-b094-4cee-bdfb-2e7b0ac28dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae7ef37d-1c1e-42b9-bb6e-0a8a2c879a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a847c9a9-b094-4cee-bdfb-2e7b0ac28dba",
                    "LayerId": "c7dca445-1e65-438d-81e5-622906dda5db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "c7dca445-1e65-438d-81e5-622906dda5db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27f74558-22a4-43ef-b09f-2f3e105cde3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 17
}