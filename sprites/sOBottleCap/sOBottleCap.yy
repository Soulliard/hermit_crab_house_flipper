{
    "id": "195b0f77-f412-4564-acf6-c1b7d23094da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOBottleCap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 58,
    "bbox_right": 77,
    "bbox_top": 47,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a9c19e1-fe1f-4cef-9e49-3d59c052b473",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "195b0f77-f412-4564-acf6-c1b7d23094da",
            "compositeImage": {
                "id": "ff741520-f128-443a-a0fe-5b3f6bb67284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9c19e1-fe1f-4cef-9e49-3d59c052b473",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee1b793-e073-4371-94ec-f4aa82d08914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9c19e1-fe1f-4cef-9e49-3d59c052b473",
                    "LayerId": "80c5a9d5-ec1e-4f4b-bcb7-8d466d91172e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "80c5a9d5-ec1e-4f4b-bcb7-8d466d91172e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "195b0f77-f412-4564-acf6-c1b7d23094da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}