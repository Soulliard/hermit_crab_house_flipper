{
    "id": "3da4ab83-0aea-4e49-816d-4f3fbadc775d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0fa05be-0e67-44e9-a034-ab5a92dc2407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3da4ab83-0aea-4e49-816d-4f3fbadc775d",
            "compositeImage": {
                "id": "955e33a2-8ba8-439f-b95a-47921a736f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0fa05be-0e67-44e9-a034-ab5a92dc2407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f82d78d-1a7a-4361-a1fe-607d2dc09937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0fa05be-0e67-44e9-a034-ab5a92dc2407",
                    "LayerId": "021d7779-1231-4fb6-aed5-c2d4547db666"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "021d7779-1231-4fb6-aed5-c2d4547db666",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3da4ab83-0aea-4e49-816d-4f3fbadc775d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}