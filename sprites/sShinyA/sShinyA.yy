{
    "id": "dfef6b58-e2f7-40bd-960d-6d12a6a8d5f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShinyA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 27,
    "bbox_right": 31,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7f5a2cb-d2ea-4609-ab38-588717ba41e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfef6b58-e2f7-40bd-960d-6d12a6a8d5f5",
            "compositeImage": {
                "id": "330539bf-680a-4238-b614-1272caad0229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f5a2cb-d2ea-4609-ab38-588717ba41e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "976d5f1d-4b1f-4e80-a715-4dd70163f393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f5a2cb-d2ea-4609-ab38-588717ba41e7",
                    "LayerId": "2bee66b4-e5d5-4d17-a1aa-884dd8e3b3c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2bee66b4-e5d5-4d17-a1aa-884dd8e3b3c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfef6b58-e2f7-40bd-960d-6d12a6a8d5f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -18,
    "yorig": 15
}