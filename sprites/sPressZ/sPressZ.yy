{
    "id": "a1861281-2bd7-4a52-9622-3ef1c26c0495",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPressZ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3915ee41-afcd-4fd9-b7e8-1f6b6151fbea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1861281-2bd7-4a52-9622-3ef1c26c0495",
            "compositeImage": {
                "id": "04be5df5-90ed-4ffa-a45a-2a84b03f4f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3915ee41-afcd-4fd9-b7e8-1f6b6151fbea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f94da5a5-b1e0-463d-bf90-145cf220094f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3915ee41-afcd-4fd9-b7e8-1f6b6151fbea",
                    "LayerId": "6d5c5d07-c720-424e-b75d-b1e092b771be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "6d5c5d07-c720-424e-b75d-b1e092b771be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1861281-2bd7-4a52-9622-3ef1c26c0495",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 15
}