{
    "id": "2ef87c35-4ce1-4ccc-bf90-f493ac81ee29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHealth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc1b61b3-a03c-47be-b9e1-1d4e8865ac07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ef87c35-4ce1-4ccc-bf90-f493ac81ee29",
            "compositeImage": {
                "id": "4f8b3382-dd26-44cc-ab3b-f6e9d011c971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1b61b3-a03c-47be-b9e1-1d4e8865ac07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b811cfd2-f294-41bb-ba41-859259f27deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1b61b3-a03c-47be-b9e1-1d4e8865ac07",
                    "LayerId": "d0cef1a4-fc11-4724-812d-e49ccb66f6ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d0cef1a4-fc11-4724-812d-e49ccb66f6ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ef87c35-4ce1-4ccc-bf90-f493ac81ee29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}