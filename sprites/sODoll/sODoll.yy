{
    "id": "19c95e7f-5528-4267-9f19-3b99e4e602f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sODoll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 69,
    "bbox_right": 105,
    "bbox_top": 47,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae0be638-114e-4df9-b869-916dafbf6be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19c95e7f-5528-4267-9f19-3b99e4e602f3",
            "compositeImage": {
                "id": "b8a76dbe-0d10-4e7b-8abe-6a456ce7b6fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae0be638-114e-4df9-b869-916dafbf6be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80decf33-a5b2-4983-aa38-faa69512a2fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae0be638-114e-4df9-b869-916dafbf6be9",
                    "LayerId": "f080b8cc-7896-445a-8120-01fe61afde93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "f080b8cc-7896-445a-8120-01fe61afde93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19c95e7f-5528-4267-9f19-3b99e4e602f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}