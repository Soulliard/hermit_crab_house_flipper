{
    "id": "8594c873-6143-4dc5-8e3d-25d2c2ed427e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBottleCap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02f1a9d3-526f-47e2-825b-436287849302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594c873-6143-4dc5-8e3d-25d2c2ed427e",
            "compositeImage": {
                "id": "be004a4c-a5c5-43de-b2b4-c9838eb9471c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f1a9d3-526f-47e2-825b-436287849302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b47422bd-b32d-432e-8adc-8b9b73195b21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f1a9d3-526f-47e2-825b-436287849302",
                    "LayerId": "abc0f476-2812-4fc5-aefb-fc15e303696b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "abc0f476-2812-4fc5-aefb-fc15e303696b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8594c873-6143-4dc5-8e3d-25d2c2ed427e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 0,
    "yorig": 0
}