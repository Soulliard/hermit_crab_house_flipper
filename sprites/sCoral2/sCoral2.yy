{
    "id": "114260c3-2596-412a-956a-89afdca54372",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCoral2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06bd1c88-7ff8-4cce-929b-49da3ed14f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "114260c3-2596-412a-956a-89afdca54372",
            "compositeImage": {
                "id": "1b072987-5528-46dc-97ef-901a365b566c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06bd1c88-7ff8-4cce-929b-49da3ed14f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bff3c217-dd4a-4e5a-8795-4b13a4f307ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06bd1c88-7ff8-4cce-929b-49da3ed14f95",
                    "LayerId": "82926015-ae65-4db4-8d13-1e69301aae0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "82926015-ae65-4db4-8d13-1e69301aae0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "114260c3-2596-412a-956a-89afdca54372",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 0,
    "yorig": 0
}