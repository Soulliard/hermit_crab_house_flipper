{
    "id": "215f84fb-a914-4b2f-80f9-e1dd28837a2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sORings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 64,
    "bbox_right": 98,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe0a5772-1874-4dd5-88b7-1b9b90a328c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "215f84fb-a914-4b2f-80f9-e1dd28837a2f",
            "compositeImage": {
                "id": "eb9009db-5d4f-4cba-8594-b3b36db40b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe0a5772-1874-4dd5-88b7-1b9b90a328c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d352c1cf-6849-4eb3-a0af-c3866862d36e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe0a5772-1874-4dd5-88b7-1b9b90a328c0",
                    "LayerId": "f4b26b57-e96d-451e-9117-0018c57a80cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "f4b26b57-e96d-451e-9117-0018c57a80cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "215f84fb-a914-4b2f-80f9-e1dd28837a2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}