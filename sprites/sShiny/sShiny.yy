{
    "id": "317539e6-1025-445f-8f25-1692e0094f00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShiny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 31,
    "bbox_right": 35,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "446f1655-cbea-4b04-b750-18494f55aa7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "317539e6-1025-445f-8f25-1692e0094f00",
            "compositeImage": {
                "id": "d4f5d141-a27e-4447-beee-d25836e63305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "446f1655-cbea-4b04-b750-18494f55aa7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc337fe-5325-40f0-9d79-a18bcee70e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "446f1655-cbea-4b04-b750-18494f55aa7b",
                    "LayerId": "4f0bdbe3-460d-4616-a2ca-aa4de32f6c64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4f0bdbe3-460d-4616-a2ca-aa4de32f6c64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "317539e6-1025-445f-8f25-1692e0094f00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 19,
    "yorig": 16
}