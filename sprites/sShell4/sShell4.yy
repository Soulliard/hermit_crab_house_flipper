{
    "id": "ff9c7df0-3244-4849-b3e7-0ba8b1078e3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShell4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 19,
    "bbox_right": 121,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a967cb7-3189-4a61-9fd6-c211051dc066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff9c7df0-3244-4849-b3e7-0ba8b1078e3e",
            "compositeImage": {
                "id": "89225f77-bfa8-498d-a24e-459efd9ff830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a967cb7-3189-4a61-9fd6-c211051dc066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c587de7-0a2c-457b-a18e-52ce86b5d750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a967cb7-3189-4a61-9fd6-c211051dc066",
                    "LayerId": "fdb3a7dc-b605-4902-a3aa-d3e41609f6a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "fdb3a7dc-b605-4902-a3aa-d3e41609f6a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff9c7df0-3244-4849-b3e7-0ba8b1078e3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}