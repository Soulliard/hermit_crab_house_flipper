{
    "id": "ce641023-6ea5-4a38-8c10-e79db6e83233",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUrchinBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9757403-a167-489d-9f75-2c042c6d7408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce641023-6ea5-4a38-8c10-e79db6e83233",
            "compositeImage": {
                "id": "7d51fdc8-9033-4a0a-9326-752c63e941ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9757403-a167-489d-9f75-2c042c6d7408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf99249c-04c0-4a8a-b501-4e418c880f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9757403-a167-489d-9f75-2c042c6d7408",
                    "LayerId": "d54c7e72-1cac-4e72-aad8-e61e69c73dd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "d54c7e72-1cac-4e72-aad8-e61e69c73dd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce641023-6ea5-4a38-8c10-e79db6e83233",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 3
}