{
    "id": "95420960-a33c-4397-b5f8-613e1a967256",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanTab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15e33583-e865-49fc-a7f5-79b529708e93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95420960-a33c-4397-b5f8-613e1a967256",
            "compositeImage": {
                "id": "0aeddfc0-d7de-4ba9-84c2-9bf72bcdc56c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15e33583-e865-49fc-a7f5-79b529708e93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87cd7526-e27d-48b0-b1a3-3fc4ade5399e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15e33583-e865-49fc-a7f5-79b529708e93",
                    "LayerId": "b540f30a-3f84-412a-8c91-16d47eed361a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "b540f30a-3f84-412a-8c91-16d47eed361a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95420960-a33c-4397-b5f8-613e1a967256",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 0
}