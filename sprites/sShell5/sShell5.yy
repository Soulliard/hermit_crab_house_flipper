{
    "id": "1e949759-8f45-4431-add0-4d7e576ac38f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShell5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 31,
    "bbox_right": 98,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cde0f9aa-9d29-4d41-8c40-d789c83c9d79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e949759-8f45-4431-add0-4d7e576ac38f",
            "compositeImage": {
                "id": "6dbe3f17-8764-47a5-9ff9-138293818271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cde0f9aa-9d29-4d41-8c40-d789c83c9d79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49499224-b16e-4830-ba0b-9359b20020bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cde0f9aa-9d29-4d41-8c40-d789c83c9d79",
                    "LayerId": "4fb92733-cdfa-4bd0-bbf4-880d44124763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "4fb92733-cdfa-4bd0-bbf4-880d44124763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e949759-8f45-4431-add0-4d7e576ac38f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}