{
    "id": "9534dd95-c40c-4010-97b0-6f05af9ba8b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8cef01f-224c-407d-a19d-e7055c37ba39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9534dd95-c40c-4010-97b0-6f05af9ba8b7",
            "compositeImage": {
                "id": "b009ed08-ccdf-4bfc-928b-0ef477db00b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8cef01f-224c-407d-a19d-e7055c37ba39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b310601b-0b7d-4147-b907-64ede3b74b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8cef01f-224c-407d-a19d-e7055c37ba39",
                    "LayerId": "acc8119f-9eba-4f51-a3db-187493141a83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "acc8119f-9eba-4f51-a3db-187493141a83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9534dd95-c40c-4010-97b0-6f05af9ba8b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}