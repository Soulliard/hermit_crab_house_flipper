{
    "id": "dda1ff83-0182-4df5-bf6f-6e9725f7c577",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShell1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 23,
    "bbox_right": 98,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "920c78ac-8e88-4897-af77-22d97a497f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dda1ff83-0182-4df5-bf6f-6e9725f7c577",
            "compositeImage": {
                "id": "3e14aa40-fbe2-495a-85d4-3c6692174635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "920c78ac-8e88-4897-af77-22d97a497f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab5a35ed-15ac-4f1f-bf3b-1fb404ed3b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "920c78ac-8e88-4897-af77-22d97a497f51",
                    "LayerId": "2dddc68f-a0a7-422e-b746-f3298c21273b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "2dddc68f-a0a7-422e-b746-f3298c21273b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dda1ff83-0182-4df5-bf6f-6e9725f7c577",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}