{
    "id": "c0e8c56f-f70e-4774-9956-aef4ff70dd21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKelp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24342204-1cbb-4301-ad8e-c9444d308709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0e8c56f-f70e-4774-9956-aef4ff70dd21",
            "compositeImage": {
                "id": "dbc847b1-54c4-4e37-aa75-e5b55d32ab74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24342204-1cbb-4301-ad8e-c9444d308709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d4bde4-cadf-44f5-88e1-d6f003f120e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24342204-1cbb-4301-ad8e-c9444d308709",
                    "LayerId": "e95e6b04-4d7b-4412-a098-d4d1b3ca9021"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 67,
    "layers": [
        {
            "id": "e95e6b04-4d7b-4412-a098-d4d1b3ca9021",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0e8c56f-f70e-4774-9956-aef4ff70dd21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}