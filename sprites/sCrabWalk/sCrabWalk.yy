{
    "id": "d1b03603-f096-45e6-abf4-44042e1419db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrabWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d15f7f4e-25b0-4eae-b622-78deffb36bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b03603-f096-45e6-abf4-44042e1419db",
            "compositeImage": {
                "id": "e2fd029b-cce9-44b9-8f7f-9b75379c225e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d15f7f4e-25b0-4eae-b622-78deffb36bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "895d010a-1c7b-4254-ac91-ec7e8bbc11e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d15f7f4e-25b0-4eae-b622-78deffb36bc9",
                    "LayerId": "167aced4-b60b-4582-b103-1bc48215a40a"
                }
            ]
        },
        {
            "id": "72bc1351-bae8-4182-8fa7-63f6543e63cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b03603-f096-45e6-abf4-44042e1419db",
            "compositeImage": {
                "id": "ebb1c4fa-cee6-444f-b384-f9a7cd5ed76e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72bc1351-bae8-4182-8fa7-63f6543e63cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38c357ec-d43d-48d2-a766-df913e75d6c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72bc1351-bae8-4182-8fa7-63f6543e63cd",
                    "LayerId": "167aced4-b60b-4582-b103-1bc48215a40a"
                }
            ]
        },
        {
            "id": "21a07243-acf4-466c-938b-4d6180fdcc20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b03603-f096-45e6-abf4-44042e1419db",
            "compositeImage": {
                "id": "0968c3e3-dbef-41fe-b24b-abf8f2a85840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21a07243-acf4-466c-938b-4d6180fdcc20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876b4050-0ff8-476e-9fd8-6db60cd77589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21a07243-acf4-466c-938b-4d6180fdcc20",
                    "LayerId": "167aced4-b60b-4582-b103-1bc48215a40a"
                }
            ]
        },
        {
            "id": "5b417c6c-2e15-4615-b054-a661bbc8e36e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b03603-f096-45e6-abf4-44042e1419db",
            "compositeImage": {
                "id": "a35f6bb5-ae1c-4893-9d22-64a43a56e572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b417c6c-2e15-4615-b054-a661bbc8e36e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e5b80fb-d42b-4735-81cb-26a44d89884e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b417c6c-2e15-4615-b054-a661bbc8e36e",
                    "LayerId": "167aced4-b60b-4582-b103-1bc48215a40a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "167aced4-b60b-4582-b103-1bc48215a40a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1b03603-f096-45e6-abf4-44042e1419db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 17
}