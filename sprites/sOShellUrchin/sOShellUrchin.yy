{
    "id": "13f41f0d-f145-4196-97ee-9c5abe7b4025",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOShellUrchin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 83,
    "bbox_right": 121,
    "bbox_top": 51,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a004c34-b757-43b2-aae2-cb1a7ad6c401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f41f0d-f145-4196-97ee-9c5abe7b4025",
            "compositeImage": {
                "id": "e986a606-8af2-45b5-8ec4-318a3e0c7408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a004c34-b757-43b2-aae2-cb1a7ad6c401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "641aff4a-969d-46d6-8d34-e71df93c39d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a004c34-b757-43b2-aae2-cb1a7ad6c401",
                    "LayerId": "8822e50e-6806-4c21-99bd-ce8dda76f0c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "8822e50e-6806-4c21-99bd-ce8dda76f0c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13f41f0d-f145-4196-97ee-9c5abe7b4025",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}