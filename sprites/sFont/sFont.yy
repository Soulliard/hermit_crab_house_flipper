{
    "id": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFont",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59d5be2f-527a-4f37-974d-ec7fe02f0b2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "d56667c9-96d2-41b9-ae41-c9fc7a7bf77c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59d5be2f-527a-4f37-974d-ec7fe02f0b2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a8ab9c9-4b8e-48d4-ab96-5972153c4156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59d5be2f-527a-4f37-974d-ec7fe02f0b2f",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "a19d53e8-df5e-4c1f-bb1a-dae7b0997bd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "9b432e8f-4bda-46ef-85f6-a537b687752a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a19d53e8-df5e-4c1f-bb1a-dae7b0997bd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fea4a8f-758e-4464-bd12-4658f8979063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a19d53e8-df5e-4c1f-bb1a-dae7b0997bd3",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "cbbd553f-fbe3-4d1f-b006-71d59b8d17e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "555ac345-d8c5-454a-b646-174cce6179cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbbd553f-fbe3-4d1f-b006-71d59b8d17e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46380555-52d0-4d35-92e4-968867f4cc01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbbd553f-fbe3-4d1f-b006-71d59b8d17e4",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "edcaef1b-0849-4590-8b4b-b4c7f2914a7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "91e46eec-40ff-4fd8-a4cf-a0df5a467bcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edcaef1b-0849-4590-8b4b-b4c7f2914a7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f7f3f0-41f6-4c83-81f2-ddc3a2a24de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edcaef1b-0849-4590-8b4b-b4c7f2914a7e",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "dee75dc6-804a-4986-bc02-0d3d45df0431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "a86f0b1b-35f1-41fe-9bbc-cfe0b728fa6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee75dc6-804a-4986-bc02-0d3d45df0431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474f027a-4e06-475b-9282-091a0f710818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee75dc6-804a-4986-bc02-0d3d45df0431",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "ceb61a40-aa6f-4d2c-8678-2f567fdc3470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "d8c2654f-81ac-4034-956c-ed0fef5409c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceb61a40-aa6f-4d2c-8678-2f567fdc3470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "597c8254-8b4e-4752-add0-04a09c82648b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceb61a40-aa6f-4d2c-8678-2f567fdc3470",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "c79c28e7-d970-444b-aeb1-cdf5dbc33f59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "b243abb1-1e14-468f-8cfb-638bc681eebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c79c28e7-d970-444b-aeb1-cdf5dbc33f59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15bb8d21-e875-4e06-8b7f-46a9c04c022b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c79c28e7-d970-444b-aeb1-cdf5dbc33f59",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "b5e73ecc-fbbc-40ab-b92f-78c011db5a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "dfa4ad22-4840-4bcc-8c37-7e40d65f07ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e73ecc-fbbc-40ab-b92f-78c011db5a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbe23392-0590-4e88-8b50-832d8a7c2378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e73ecc-fbbc-40ab-b92f-78c011db5a19",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "da0649e7-eb57-4ca6-ad45-ac354aa87571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "1bc0654a-795a-4502-8922-626347a2865f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da0649e7-eb57-4ca6-ad45-ac354aa87571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ba3afd-4f94-4649-b226-baba8c1407ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da0649e7-eb57-4ca6-ad45-ac354aa87571",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "555fb2db-6c8e-4449-a417-68ece77365ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "6a5a3dcb-7987-471d-9849-d900a39df2de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "555fb2db-6c8e-4449-a417-68ece77365ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "139ceecb-9a61-4333-89e1-b93d33b86b36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "555fb2db-6c8e-4449-a417-68ece77365ec",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "aac6038f-05fb-4ab7-aa7f-0e46783fe0ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "c3703883-c1ee-4cb6-86c2-2fe0bda53926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aac6038f-05fb-4ab7-aa7f-0e46783fe0ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e32ebc-b2c9-4537-8f99-087aba4b8619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aac6038f-05fb-4ab7-aa7f-0e46783fe0ec",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "9fbfa087-f953-4527-a466-9652cd3fdb05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "470b426a-d7ce-48ff-9245-1ab4a3457b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fbfa087-f953-4527-a466-9652cd3fdb05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e94e3369-e681-4571-8e81-ca6ca975e236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fbfa087-f953-4527-a466-9652cd3fdb05",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "09d49a33-cdf7-49bd-aafb-f3ffe3ce3a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "29dd695f-f61a-4b5c-b9ab-eac4dfb2a433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09d49a33-cdf7-49bd-aafb-f3ffe3ce3a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c79882-ed33-428d-955a-7f1ba2045f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09d49a33-cdf7-49bd-aafb-f3ffe3ce3a69",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "b6ef80fe-291f-4d50-bc64-fba91bcebc5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "729c0e43-884e-4d57-92ce-7c32f465ba5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6ef80fe-291f-4d50-bc64-fba91bcebc5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb21bcc-119c-47c3-b740-fa62de6184b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6ef80fe-291f-4d50-bc64-fba91bcebc5c",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "df84a693-c7af-45a7-b217-8cdf1a1fbecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "72cd21a3-e7ed-4c9b-b10f-869fb6fe2a01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df84a693-c7af-45a7-b217-8cdf1a1fbecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56bda4f8-9a7b-495d-894d-0a525c84d4f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df84a693-c7af-45a7-b217-8cdf1a1fbecd",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "a93cd0cb-ca38-4c0f-8621-555b33a9eef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "9b28755a-a5a4-408b-9e85-ed8dee412a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a93cd0cb-ca38-4c0f-8621-555b33a9eef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d46f277b-c8e8-4120-a80d-6751085bdc2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a93cd0cb-ca38-4c0f-8621-555b33a9eef5",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "e6c78120-9d9b-497b-8eba-69cfac1a2ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "f07e74f4-e074-4373-83db-c97184b77c30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6c78120-9d9b-497b-8eba-69cfac1a2ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a343edf1-cdbb-4acf-92c6-2497218c58fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6c78120-9d9b-497b-8eba-69cfac1a2ad6",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "5ca3d5cc-5d64-4a25-8d98-ac89bb00b626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "71d8840d-3099-4fe4-b419-b59b1a29f3d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ca3d5cc-5d64-4a25-8d98-ac89bb00b626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f647d5f-d015-4dee-bf9f-7fb3310d3391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ca3d5cc-5d64-4a25-8d98-ac89bb00b626",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "6669942a-1124-4f84-8446-60418829e39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "d0f0f64c-8f40-4e09-b06e-e801df1b32e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6669942a-1124-4f84-8446-60418829e39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac101ef-572d-44cf-a4a8-3baa2eee6910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6669942a-1124-4f84-8446-60418829e39a",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "1b8c2bcd-6031-4841-bf15-f848c0188a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "6c7213cf-55a5-4ad4-8d63-7ed57e0ad1a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8c2bcd-6031-4841-bf15-f848c0188a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299c3adf-f781-4b80-9e45-4663015d0b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8c2bcd-6031-4841-bf15-f848c0188a3b",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "7598f166-4a16-4f6c-84f8-22e8ff41bef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "df87ac8a-940b-4862-b5fc-41054e5e994b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7598f166-4a16-4f6c-84f8-22e8ff41bef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "320c1036-e19d-4397-980a-2ca2f7e2531c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7598f166-4a16-4f6c-84f8-22e8ff41bef6",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "2230d8a3-2f84-4d1a-b411-03148ebe7af0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "72ef91f9-0a97-4812-840b-29bcda3da051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2230d8a3-2f84-4d1a-b411-03148ebe7af0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1eac663-4b4e-4c35-8df2-eb063487b92b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2230d8a3-2f84-4d1a-b411-03148ebe7af0",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "3d06d966-1fae-4b96-b557-bc0a1f0203a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "b7a1f01d-ed2e-434f-9e61-270b4c302fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d06d966-1fae-4b96-b557-bc0a1f0203a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f473177c-bf35-4dfc-9473-db65a676b3b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d06d966-1fae-4b96-b557-bc0a1f0203a6",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "06dc053e-5536-46c3-bed4-72d18c7157d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "0fbf0cac-c923-4a5a-a026-84b106d18cd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06dc053e-5536-46c3-bed4-72d18c7157d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fdb86c7-e591-4d45-8e2d-50959e02114d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06dc053e-5536-46c3-bed4-72d18c7157d7",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "735d8b0c-36e1-405d-9229-692a0a71a669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "5c1d065c-8c54-44e6-862d-a784fed6d352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "735d8b0c-36e1-405d-9229-692a0a71a669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e08de8-8e6c-42b1-896f-2bd623223b74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "735d8b0c-36e1-405d-9229-692a0a71a669",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "3f5f099d-0cba-4249-829f-74491faa0cb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "f2185ceb-6435-4a2c-af81-1df70bc68259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5f099d-0cba-4249-829f-74491faa0cb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f62ac12-a47f-481e-ab88-0afe56310aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5f099d-0cba-4249-829f-74491faa0cb6",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "8a229b44-0d1d-4b7e-8df0-46cce5ca0694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "44d6d7de-9f95-44bc-ab82-a689660910bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a229b44-0d1d-4b7e-8df0-46cce5ca0694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa7cd64-c17d-4d63-b062-756fcbe3fc13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a229b44-0d1d-4b7e-8df0-46cce5ca0694",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "666c4e66-96e3-4fea-8111-e866621f90eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "5a318730-b425-4c6b-bc6c-80571f5e3d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666c4e66-96e3-4fea-8111-e866621f90eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b5ed0e-d8a0-42d3-bced-a3cc5344438c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666c4e66-96e3-4fea-8111-e866621f90eb",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "6265712f-e655-4db4-9021-1e6cda6df8c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "5d728c9a-64b5-4b9b-8ee7-e7cea0c4bea5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6265712f-e655-4db4-9021-1e6cda6df8c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f6070a-67a0-453a-9066-b1848b618eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6265712f-e655-4db4-9021-1e6cda6df8c2",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "f0e149d1-c5c5-4519-9049-38f87138d300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "7deec823-2dfc-4ba3-9e1f-fc2f495ac481",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e149d1-c5c5-4519-9049-38f87138d300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a46b51-fae5-4eb7-9844-173a4665489f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e149d1-c5c5-4519-9049-38f87138d300",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "61b5a459-2af8-4fd0-a14e-2ba07bfb49cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "3cfa3d0b-e758-4f73-9353-dc6d0457e757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61b5a459-2af8-4fd0-a14e-2ba07bfb49cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b31389-8f01-4aa3-a4d8-07bb574da2c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61b5a459-2af8-4fd0-a14e-2ba07bfb49cc",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "3d09e2db-bf75-416a-a657-b5afa296fa4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "587e259f-54e9-4ca8-970d-0ee1ced29eb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d09e2db-bf75-416a-a657-b5afa296fa4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "584d80a3-33d6-485e-9350-2aeac4ebb7ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d09e2db-bf75-416a-a657-b5afa296fa4a",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "02b06182-53bb-4e99-b1d5-fd98c1fd27ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "23265cb2-abf2-4044-9c13-cc5abd1dcb97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b06182-53bb-4e99-b1d5-fd98c1fd27ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d58abf-4873-4065-b07e-40993b709b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b06182-53bb-4e99-b1d5-fd98c1fd27ee",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "de23cd26-3118-4508-8ad3-bbeccdf6278b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "8a2986a5-47da-4608-83c3-9e39e3c00f4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de23cd26-3118-4508-8ad3-bbeccdf6278b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd08599e-8f70-425c-9f55-87f36ca819fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de23cd26-3118-4508-8ad3-bbeccdf6278b",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "ff9492f0-8913-4aac-afd9-86680e12ade8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "0e5a7e61-c10a-4dfd-952e-96a24bd5046b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff9492f0-8913-4aac-afd9-86680e12ade8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa92234-e0ec-45b2-9e6d-f9183d0c4056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff9492f0-8913-4aac-afd9-86680e12ade8",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "6b2bc37a-26c2-4254-87f2-433279edc376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "288c1599-4dcb-4d94-a9c2-4bc44be5278d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2bc37a-26c2-4254-87f2-433279edc376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63db4e5c-0982-44a4-878a-4d886b7747f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2bc37a-26c2-4254-87f2-433279edc376",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "39007535-1335-4b39-9fb8-030818fcedf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "78e88176-c01f-441e-9fb4-452e8eb7d8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39007535-1335-4b39-9fb8-030818fcedf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2247df2f-d0ab-46a0-93cd-09a3d7a538bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39007535-1335-4b39-9fb8-030818fcedf9",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "85974201-4015-4bc6-b261-a1e023f7df4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "98cd97cb-265d-45e3-8f45-b50145f972c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85974201-4015-4bc6-b261-a1e023f7df4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72f2e9f7-4630-4245-898b-c31b64c31329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85974201-4015-4bc6-b261-a1e023f7df4f",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "d46b182f-4d9c-4d76-9b3f-af1fa1ef00b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "ae7c8279-2f56-4757-9031-988a581f83d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46b182f-4d9c-4d76-9b3f-af1fa1ef00b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c551a3-2038-4295-a8b3-30d34fb5ac49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46b182f-4d9c-4d76-9b3f-af1fa1ef00b8",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "0f541fd5-3d67-4b99-aa4b-e59f5150c7f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "4ecab7dd-9895-4e42-82a8-16787532a832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f541fd5-3d67-4b99-aa4b-e59f5150c7f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7ee92a-2f40-423c-b742-66f7da3acc1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f541fd5-3d67-4b99-aa4b-e59f5150c7f0",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "42401eaa-2c8c-4d0e-91f4-b7e4b3bb7f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "4bf317cb-d2b6-4060-bf8b-16a9642edafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42401eaa-2c8c-4d0e-91f4-b7e4b3bb7f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54b82948-9d25-4114-b2ec-00837bbd9596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42401eaa-2c8c-4d0e-91f4-b7e4b3bb7f37",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "2929ba01-79a3-4ee8-b802-1f0f7317bdde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "6de59cbc-d86e-4312-b6ed-a52cbab08a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2929ba01-79a3-4ee8-b802-1f0f7317bdde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62ecd8b4-b687-47b0-ac45-65d4a4d7f753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2929ba01-79a3-4ee8-b802-1f0f7317bdde",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "f310a375-dd6c-4823-8056-4dd65d21901d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "e4dc23f4-c87f-4ef8-9b5f-70d62176d536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f310a375-dd6c-4823-8056-4dd65d21901d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4738d527-abfe-49b3-8286-599ffb2aa8d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f310a375-dd6c-4823-8056-4dd65d21901d",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "f77ef825-3d57-4133-b88c-8bbe3eaab317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "652793cc-1dc5-4cce-a101-62947b033a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77ef825-3d57-4133-b88c-8bbe3eaab317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f3b7e0-2776-4518-a083-909c59cf34ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77ef825-3d57-4133-b88c-8bbe3eaab317",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "c1f5d727-ceda-420b-91e3-cd28cfda00af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "494f4b65-6553-424c-aeb0-1741d5d3130d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f5d727-ceda-420b-91e3-cd28cfda00af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "720d88e0-7258-4640-a73b-ada5179e408d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f5d727-ceda-420b-91e3-cd28cfda00af",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "33874cdd-589b-464c-816e-6a9d36d52cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "3da08193-6276-4a21-a44c-62f1339491a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33874cdd-589b-464c-816e-6a9d36d52cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c2bcb3-9471-4de4-976e-1d809fa70ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33874cdd-589b-464c-816e-6a9d36d52cbb",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "7ea09d54-a6a2-4795-a25e-ac4a1b8608de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "c6143707-40b9-4fec-85bd-5e4f408a6673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ea09d54-a6a2-4795-a25e-ac4a1b8608de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43995910-a7f0-4645-9da7-fc64412596b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ea09d54-a6a2-4795-a25e-ac4a1b8608de",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "a38ac37f-93fc-4510-9b50-1c2b4d4d12d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "fc94cc48-0837-4e01-8d8f-5f412723ce69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a38ac37f-93fc-4510-9b50-1c2b4d4d12d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1916aff-0f7f-4d6f-a8dd-474f7795f003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a38ac37f-93fc-4510-9b50-1c2b4d4d12d2",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "ec2336a0-6591-4952-8d99-0ebf242add36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "2d0a36b1-a505-42a7-aa10-8ae5dffc1f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2336a0-6591-4952-8d99-0ebf242add36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b751c2-4f27-406e-8204-e3c0059c5782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2336a0-6591-4952-8d99-0ebf242add36",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "630d1192-67fa-474b-b3cb-54995f9cc684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "bdcf3d46-b322-4df3-87f9-304ba15b5023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "630d1192-67fa-474b-b3cb-54995f9cc684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bce5bee-0d28-4fcf-b21b-53d09a9d535c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "630d1192-67fa-474b-b3cb-54995f9cc684",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "d090572e-d9a9-491b-b1f6-ed98ada97abe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "ca973fd9-f711-4b32-b95f-5fb0ccd287fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d090572e-d9a9-491b-b1f6-ed98ada97abe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "254ba8c9-7b3c-4e75-b060-a7d0210dd5bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d090572e-d9a9-491b-b1f6-ed98ada97abe",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "48df2cb2-a2a0-4dcc-90ce-f7b19b79361e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "0b9aa425-59dd-4fa8-b4f3-89f4f931d70d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48df2cb2-a2a0-4dcc-90ce-f7b19b79361e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3fab03-e089-4f64-b12d-0e27bff0f931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48df2cb2-a2a0-4dcc-90ce-f7b19b79361e",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "c7726a2d-074a-44e9-a1ae-260ced9c4fa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "3a6dcb9c-c87f-45bc-a402-7b484f2435e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7726a2d-074a-44e9-a1ae-260ced9c4fa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f1e3e4-7cd7-4df0-8f5a-a7a9c42f3cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7726a2d-074a-44e9-a1ae-260ced9c4fa2",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "426b68ca-4d10-4f18-a10f-eb46931203d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "f4728a0f-302b-4c8a-8bbf-70524861eb39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426b68ca-4d10-4f18-a10f-eb46931203d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b27bdd-4edc-4c5a-8ec9-2506ad3d6617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426b68ca-4d10-4f18-a10f-eb46931203d3",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "37b732e6-90b8-4d1a-8bd9-4e9e18e2c173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "74e39fc8-9fb8-4691-b35e-4742290a998e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b732e6-90b8-4d1a-8bd9-4e9e18e2c173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b426a99c-b083-40fb-9980-da4588c46b85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b732e6-90b8-4d1a-8bd9-4e9e18e2c173",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "16e542ea-973f-4df6-b654-af911161e695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "f48a6aad-3eff-4617-82d5-9bc88db8177e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16e542ea-973f-4df6-b654-af911161e695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17d0576-0b4f-44b3-8699-1c56cf292e54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16e542ea-973f-4df6-b654-af911161e695",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "c468ad35-1b58-4559-b15c-49a03bc6e633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "66f53d1c-fb30-488c-aea3-a7cb58f43bf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c468ad35-1b58-4559-b15c-49a03bc6e633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faba778b-4ffb-4241-8dc8-774cbdcafa4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c468ad35-1b58-4559-b15c-49a03bc6e633",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "f42ee47d-de68-4723-a410-7cde5224cb93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "87a8bba1-4a13-4c02-a084-b722120df91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f42ee47d-de68-4723-a410-7cde5224cb93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a05fd9c9-97ce-45ec-adbb-29fb3dfa9d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f42ee47d-de68-4723-a410-7cde5224cb93",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "fa53c594-ca91-4e1f-b152-b07d4c0ed350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "81336f84-7122-48fa-95cf-26dbd7d52a5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa53c594-ca91-4e1f-b152-b07d4c0ed350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35fabfc7-dc9e-4479-ba56-de4bd7944852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa53c594-ca91-4e1f-b152-b07d4c0ed350",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "b61bc159-e7cd-4b3f-9f82-4741cfa5b746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "425b7731-cf04-498e-b781-34fa75741733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61bc159-e7cd-4b3f-9f82-4741cfa5b746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be264cbd-05ab-4991-92c7-4d66a2b8ddf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61bc159-e7cd-4b3f-9f82-4741cfa5b746",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "307b10e0-9bcf-49f8-a46b-08bd361ee8a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "413fc063-4a19-42dc-8987-be060e59ea58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307b10e0-9bcf-49f8-a46b-08bd361ee8a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07b8346b-a4ca-4207-bbfc-c5487ddf3ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307b10e0-9bcf-49f8-a46b-08bd361ee8a4",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "e621e74f-cc71-4c6a-aa2f-fc6776984b73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "40017a44-25fc-49fe-b861-751e7e8fcd8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e621e74f-cc71-4c6a-aa2f-fc6776984b73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2253be6-84f1-43cc-9243-95b973a58d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e621e74f-cc71-4c6a-aa2f-fc6776984b73",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "240fb936-f80e-4fa5-be1c-507d32d55c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "adb188ab-cbb7-44c7-a587-d0eded0bd260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "240fb936-f80e-4fa5-be1c-507d32d55c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8951936-c90f-467b-99e8-8c8e4167e148",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240fb936-f80e-4fa5-be1c-507d32d55c72",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "bcef6401-3448-4926-87c7-42c58cca64f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "3590a069-148d-464f-87b5-03d0301b537a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcef6401-3448-4926-87c7-42c58cca64f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96e5d88-0668-4ec3-946b-2d1d566b9b3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcef6401-3448-4926-87c7-42c58cca64f6",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "abf0a486-90b6-4a9d-9915-d54c13011eb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "de71ec10-4fe9-4165-87ab-688114160376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf0a486-90b6-4a9d-9915-d54c13011eb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b055adc-a898-48bd-9f8d-5242c88b13e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf0a486-90b6-4a9d-9915-d54c13011eb0",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "440504c0-b00e-4402-a209-0833cf4083ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "d108dad2-3e30-4aa5-a2ff-1a76bd8e2604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "440504c0-b00e-4402-a209-0833cf4083ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e7b869-e103-4177-9fec-fb13aa85514e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "440504c0-b00e-4402-a209-0833cf4083ce",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "db8e1818-87ff-4c1d-9d86-709eee5be97e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "fb47847f-36f9-43a5-a987-84336b27c71a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8e1818-87ff-4c1d-9d86-709eee5be97e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf914513-139e-47e2-8742-1e9e259f89f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8e1818-87ff-4c1d-9d86-709eee5be97e",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "cecafee1-1b51-429c-8794-3953f7092d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "f48da147-b70e-41cf-b5e8-54ce8e975786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cecafee1-1b51-429c-8794-3953f7092d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c93ebed-f97c-4d3d-aff6-976565cf445d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cecafee1-1b51-429c-8794-3953f7092d3a",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "fc90fc3c-0a1a-421f-b3c3-447ec78af937",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "41206771-7c1f-4bc4-a1f3-2b1312012f5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc90fc3c-0a1a-421f-b3c3-447ec78af937",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "280b1e87-fa6b-4992-a563-b453c39bf79e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc90fc3c-0a1a-421f-b3c3-447ec78af937",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "7fc4888e-2ee9-4067-b350-d3f03ac694a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "fd6ebe50-96e0-480f-921a-1b0fff1e0130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fc4888e-2ee9-4067-b350-d3f03ac694a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ea821d-e78f-4949-9128-6c56e53ad35e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fc4888e-2ee9-4067-b350-d3f03ac694a9",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "5c558fb2-2ceb-43b9-816e-bead1a95740b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "ea1a1f2b-8e47-4641-aafe-0dcb8275776a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c558fb2-2ceb-43b9-816e-bead1a95740b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68479d6b-bbd8-4d48-93a8-9f58560f3fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c558fb2-2ceb-43b9-816e-bead1a95740b",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "6443cfb8-48b4-475c-91aa-fe40b73c9db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "5111958d-eb9f-41b1-816e-334b4a869c62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6443cfb8-48b4-475c-91aa-fe40b73c9db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5048478d-ab0d-4ddd-9099-083bfb51096f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6443cfb8-48b4-475c-91aa-fe40b73c9db8",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "ee0aafa9-3fdc-4d8e-a83d-43b54987be66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "a45563f1-0d42-47a1-b3c7-3a30c242915c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0aafa9-3fdc-4d8e-a83d-43b54987be66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae9e99c-3c2f-448a-ab35-597e4c15eb0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0aafa9-3fdc-4d8e-a83d-43b54987be66",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "a1ed2351-7f4f-4ebc-967b-1eb1c69444dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "b692c2c6-93d6-4c71-9f73-80713d457923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1ed2351-7f4f-4ebc-967b-1eb1c69444dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f7b6f0-eb83-422d-9668-7e45df9764b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1ed2351-7f4f-4ebc-967b-1eb1c69444dc",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "d3d84b49-ca5e-44e1-ac17-097523245a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "b5a2ee47-aaa9-4c63-82db-fee61fa854af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3d84b49-ca5e-44e1-ac17-097523245a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45cd6c16-9a9e-4696-b8f9-67a90e01ff06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3d84b49-ca5e-44e1-ac17-097523245a5b",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "cf644924-87ef-41bb-a6c8-5e76dc24e463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "64dae32e-81ed-4ea6-a62a-94cf1d641507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf644924-87ef-41bb-a6c8-5e76dc24e463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d6ccde3-3147-45c6-8665-c069dbc77fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf644924-87ef-41bb-a6c8-5e76dc24e463",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "1ed0cbf9-e784-4cec-b4a8-1591f697d18d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "9e3d4ad2-f349-4139-8ecb-357758ee5e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed0cbf9-e784-4cec-b4a8-1591f697d18d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3982c9aa-d203-4f56-837c-6dec3abca866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed0cbf9-e784-4cec-b4a8-1591f697d18d",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "0711c456-9b43-470e-b452-7a4163e4eae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "e19330ef-7c1f-44a7-b29c-934ea7216d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0711c456-9b43-470e-b452-7a4163e4eae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71e4f7b-a579-4c64-8c5e-cd174a72a3ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0711c456-9b43-470e-b452-7a4163e4eae2",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "029522a6-0715-405d-a709-bd14da744d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "50afba2d-af8d-4836-a828-bd5600799b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029522a6-0715-405d-a709-bd14da744d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18848984-494e-4da6-8ccd-2f744bc832bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029522a6-0715-405d-a709-bd14da744d23",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "90e46001-d944-44d6-a372-9a37da71bf04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "53cc56d6-fb44-4bef-8317-a502769f9f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e46001-d944-44d6-a372-9a37da71bf04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52159df6-b38d-4567-992b-7b75839c5f33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e46001-d944-44d6-a372-9a37da71bf04",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "3cc8f744-9aee-40b3-8508-aff1ada35698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "c2fb58dc-370d-4eb0-a4fd-8a9eb2721bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc8f744-9aee-40b3-8508-aff1ada35698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61af58f-29e6-4b29-9168-7710c0b95dc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc8f744-9aee-40b3-8508-aff1ada35698",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "bfe0f456-5c74-4181-ba46-5d7536fd8188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "b1cfcec3-2eb2-45a7-9294-4d8c2215c312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe0f456-5c74-4181-ba46-5d7536fd8188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba48768-8f77-4993-a35a-a9a44ab9ddb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe0f456-5c74-4181-ba46-5d7536fd8188",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "a72d9ef6-035b-45e8-82e1-c97774e8f828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "64b8efae-a4fb-4c75-8169-ba3bbde68e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72d9ef6-035b-45e8-82e1-c97774e8f828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c11a616-03ed-4f3f-808f-14a74f27c0eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72d9ef6-035b-45e8-82e1-c97774e8f828",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "5e3ab347-2933-453a-b358-7119516df9a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "e2f9e3ee-f381-4bd2-aa24-db6cd0deaf91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e3ab347-2933-453a-b358-7119516df9a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06652074-ab59-4f2f-af85-10bba094b9b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e3ab347-2933-453a-b358-7119516df9a3",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "efc749ea-a497-4214-a62a-9ad22041cda5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "1034b429-1952-4bc7-bbfb-e6a40f99b95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efc749ea-a497-4214-a62a-9ad22041cda5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "338e3d49-e69a-45c7-be2a-bb6f0fd12116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efc749ea-a497-4214-a62a-9ad22041cda5",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "b13348fa-54f0-4a29-b8e3-6b8a8e98814e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "cfef96cd-d658-4c64-a9d2-d6384949b3e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b13348fa-54f0-4a29-b8e3-6b8a8e98814e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87af967-c6f7-4646-bbff-e62553f18425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b13348fa-54f0-4a29-b8e3-6b8a8e98814e",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "20d716b4-f6ec-4a92-99f7-90a0c5900665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "ba0817dd-71c7-4fa7-bb1b-9554a3e8ecb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20d716b4-f6ec-4a92-99f7-90a0c5900665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a5de730-9991-4f44-a440-01c355b54265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20d716b4-f6ec-4a92-99f7-90a0c5900665",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "5e346075-4973-43cf-8571-5ab2a589ad45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "29df055b-3bbb-4b05-8245-0bed42eda54f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e346075-4973-43cf-8571-5ab2a589ad45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f0a3ad-e705-4799-b9b6-cfe748072d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e346075-4973-43cf-8571-5ab2a589ad45",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "d1e9d4ee-680f-43f3-a899-954f6b319440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "3edd55ce-da7f-4e04-ab14-54408b06853b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e9d4ee-680f-43f3-a899-954f6b319440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5897471d-7198-48d5-bce6-98051afefc24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e9d4ee-680f-43f3-a899-954f6b319440",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        },
        {
            "id": "692c1a14-0b4e-4515-b93a-c39f61e3b9e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "compositeImage": {
                "id": "5897645d-c10b-4c80-b5b5-5130360132a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692c1a14-0b4e-4515-b93a-c39f61e3b9e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c11d2f-78c2-4709-8d55-0ee838034436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692c1a14-0b4e-4515-b93a-c39f61e3b9e5",
                    "LayerId": "dfde6e83-2d16-4013-8d54-a960565b8763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "dfde6e83-2d16-4013-8d54-a960565b8763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "095bf10d-3e72-43d0-bc0b-69cccc7948ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}