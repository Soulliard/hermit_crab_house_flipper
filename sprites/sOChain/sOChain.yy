{
    "id": "6de2e6ee-5adc-40d5-8b4d-4675261a85c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOChain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 27,
    "bbox_right": 87,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3efae68b-bb5b-4690-bc1e-0912fdf3a0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6de2e6ee-5adc-40d5-8b4d-4675261a85c1",
            "compositeImage": {
                "id": "7233f9b3-39df-4197-b1cf-8d0bdbc26fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3efae68b-bb5b-4690-bc1e-0912fdf3a0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c57eddf7-f4ad-4760-bc2e-5eb9fa99fe6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3efae68b-bb5b-4690-bc1e-0912fdf3a0da",
                    "LayerId": "40f37b88-0b62-4285-97bc-35a85884dcdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "40f37b88-0b62-4285-97bc-35a85884dcdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6de2e6ee-5adc-40d5-8b4d-4675261a85c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 124
}