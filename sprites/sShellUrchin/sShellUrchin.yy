{
    "id": "b6cd97c2-3200-47c2-9ec8-7a9190fc0e20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShellUrchin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4923d0ba-931b-4620-b4fe-bec2e4e3bafd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6cd97c2-3200-47c2-9ec8-7a9190fc0e20",
            "compositeImage": {
                "id": "77eaf146-d74b-4930-b5c2-f56d6d9312bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4923d0ba-931b-4620-b4fe-bec2e4e3bafd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65cbb6d8-1987-4130-a57d-833e3b301f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4923d0ba-931b-4620-b4fe-bec2e4e3bafd",
                    "LayerId": "1da3f112-1758-484f-a4fa-b887d44732a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "1da3f112-1758-484f-a4fa-b887d44732a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6cd97c2-3200-47c2-9ec8-7a9190fc0e20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 0,
    "yorig": 0
}