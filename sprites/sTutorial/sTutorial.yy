{
    "id": "c513d2ca-2566-45cd-9e2a-72bd98ef59f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTutorial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 93,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4ef8df8-d88d-4dac-8252-93b164d80727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c513d2ca-2566-45cd-9e2a-72bd98ef59f4",
            "compositeImage": {
                "id": "1ddca983-e375-4345-bd32-edd022df3463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4ef8df8-d88d-4dac-8252-93b164d80727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "222c7c09-0418-497c-96ac-bc63f96a08ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4ef8df8-d88d-4dac-8252-93b164d80727",
                    "LayerId": "b300309a-73ae-4ec9-817c-08c7fc10c441"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "b300309a-73ae-4ec9-817c-08c7fc10c441",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c513d2ca-2566-45cd-9e2a-72bd98ef59f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 47,
    "yorig": 31
}